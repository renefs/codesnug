$(function(){ // document ready

    var window_width = $(window).width();
    var stickyTop = $('#buscador-vo').offset().top;

    $( window ).resize(function() {
        window_width = $(window).width();
        stickyTop = $('#buscador-vo').offset().top;
    });

    if ($('#buscador-vo').offset()) {
        console.log("sticky_top:"+stickyTop);
        $(window).scroll(function(){
            var windowTop = $(this).scrollTop()+50;
            var save_sidebar_width = $('#save-sidebar').width()
            if (stickyTop < windowTop && window_width > 990){
                $('#buscador-vo').css({ position: 'fixed', top: 70 , width: save_sidebar_width});
            }
            else {
                $('#buscador-vo').css('position','static');
            }

        });

    }

    form_count = $("#id_snippet_count").val();

    // get extra form count so we know what index to use for the next item.
    $("#add-another").click(function() {
        element = $(get_new_snippet(form_count));
        //element.attr('name', 'snippet_' + form_count);
        //element.attr('id', 'snippet_' + form_count);

        if(form_count==0)
            $('#fields_snippet_0').after(element);
        else $('#fields_snippet_' + (form_count-1)).after(element);
        // build element and append it to our forms container

        form_count ++;
        $("#id_snippet_count").val(form_count);
        // increment form count so our view knows to populate
        // that many fields for validation
    })

    function get_new_snippet(count){

        var field_set = $('<fieldset></fieldset>');
        field_set.attr('id','fields_snippet_'+count);
        var form_group = $('<div class="form-group"></div>');
        var toolbar = $('<div class="btn-toolbar" role="toolbar"></div>');

        field_set.append(form_group);
        form_group.append(toolbar);

        var file_name = get_file_name(count);
        var buttons_size = get_size_buttons (count);
        var language = get_language_name(count);
        var editor = get_text_editor(count);
        var remove_button = get_remove_button(count);


        toolbar.append(file_name);
        toolbar.append(buttons_size);
        toolbar.append(language);
        field_set.append(editor);
        field_set.append(remove_button);

        return field_set;
    }

    function get_size_buttons (count){

        var buttons_html = '<div class="btn-group btn-group-sm">' +
            '<button type="button" id="font_smaller_'+count+'" class="btn btn-default">' +
                '<i class="glyphicon glyphicon-chevron-down"></i><i class="glyphicon glyphicon-font"></i></button>' +
            '<button type="button" id="font_bigger_'+count+'" class="btn btn-default">' +
                '<i class="glyphicon glyphicon-chevron-up"></i><i class="glyphicon glyphicon-font"></i>' +
            '</button>' +
            '</div>';

        return buttons_html;
    }

    function get_file_name(count){
        var file_name_field = '<div class="col-xs-4 ">'+
            '<input class="form-control" id="id_snippet_title_'+count+'" maxlength="100" name="snippet_title_'+count+'" type="text" placeholder="File Name">'+
        '</div>';

        return file_name_field;
    }

    function get_language_name(count){

        var language_selector = $('#id_snippet_language_0').html();

        var selector_html = '<div class="col-xs-4 ">'+
            '<select class="form-control" name="snippet_language_'+count+'" id="id_snippet_language_'+count+'">'+
            language_selector +
            '</select>'+
            '</div>';

        return selector_html;
    }

    function get_text_editor(count){

        html_editor = '<div class="form-group">' +
            '<div id="editor_'+count+'" class="django-ace-editor">' +
            '</div>';

        html_editor+='<textarea class="form-control" cols="40" id="id_snippet_text_'+count+'"' +
            ' name="snippet_text_'+count+'" rows="15"></textarea></div>';

        html_editor+='<script type="text/javascript">' +
            '$(document).ready(function() {\n' +
                'var textarea_'+count+' = $("#id_snippet_text_'+count+'");\n' +
                'textarea_'+count+'.css({display:"none"});\n'
            ;

        html_editor +='var editor_'+count+' = ace.edit("editor_'+count+'");\n' +
            'editor_'+count+'.setTheme("ace/theme/chrome");\n' +
            'editor_'+count+'.getSession().setMode("ace/mode/text");\n';


        html_editor +=
            'editor_'+count+'.getSession().on("change", function () {\n' +
            '   textarea_'+count+'.val(editor_'+count+'.getSession().getValue());\n' +
            '});\n';
        html_editor +='textarea_'+count+'.val(editor_'+count+'.getSession().getValue());\n' +
            '$("#id_snippet_language_'+count+'").change(function () {\n' +
            '   editor_'+count+'.getSession().setMode("ace/mode/" + $(this).val());\n' +
            '});\n';


        html_editor +='var modelist_'+count+' = require("ace/ext/modelist");\n' +
            'var modeEl_'+count+' = $("#id_snippet_language_'+count+'");\n' +
            'modeEl_'+count+'.empty();\n' +
            'modelist_'+count+'.modes.forEach(function(mode) {\n' +
            '   if(mode.name==="text") var mode_default="selected";\n' +
            '   else var mode_default="";\n' +
            '   modeEl_'+count+'.append("<option value=\'"+mode.name+"\' "+mode_default+">"+mode.caption+"</option>");\n' +
            '});\n';


        html_editor +='var button_font_up_'+count+' = $("#font_bigger_'+count+'");\n' +
            'button_font_up_'+count+'.click(function() {\n' +
            '   editor_'+count+'.setFontSize(editor_'+count+'.getFontSize()+2);\n' +
            '});' +
            'var button_font_up_'+count+' = $("#font_smaller_'+count+'");\n' +
            'button_font_up_'+count+'.click(function() {\n' +
            '   editor_'+count+'.setFontSize(editor_'+count+'.getFontSize()-2);\n' +
            '});\n'+
            '});\n'  +
        '</script>';

        return html_editor;
    }

    function get_remove_button(count){
        var html = '<div class="pull-right">' +
            '<button class="btn btn-link" type="button" id="remove_snippet_'+count+'">Remove this snipplet</button>' +
            '</div>';

        html+='<script type="text/javascript">\n' +
            '$(document).ready(function() {\n';

        html += 'var remove_snippet_'+count+' = $("#remove_snippet_'+count+'");\n' +
            'remove_snippet_'+count+'.click(function() {\n' +
            '   $("#fields_snippet_'+count+'").remove();\n' +
            '   var form_count = $("#id_snippet_count").val();\n'+
            '   form_count --;\n'+
            '   $("#id_snippet_count").val(form_count);\n'+
            '});';

        html+='});\n'  +
            '</script>';

        return html;
    }

});