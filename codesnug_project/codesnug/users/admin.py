from django.contrib import admin

from codesnug.users.models import MyUser

admin.site.register(MyUser)
