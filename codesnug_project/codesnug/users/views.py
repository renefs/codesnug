from django.core.exceptions import ValidationError
from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.views.generic import ListView, CreateView, DetailView, TemplateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.contrib import messages
from django.http import Http404
import logging
from braces.views import LoginRequiredMixin
from django.utils.translation import ugettext_lazy as _

from codesnug.goals.models import WorkspacePermissions
from codesnug.users.models import UserGroup
from codesnug.settings import SELECTABLE_WORKSPACE_PERMISSIONS
from codesnug.users.forms import AddUserToGroupForm, AvatarChangeForm
from codesnug.users.models import MyUser
from codesnug.utils import *

# Get an instance of a logger
logger = logging.getLogger(__name__)


class UserGroupList(LoginRequiredMixin, ListView):
    model = UserGroup
    template_name = 'users/usergroup_list.html'

    def get_queryset(self):
        return UserGroup.objects.filter(owner=self.request.user)

    def get(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            url = reverse('goals_list', kwargs={'username': self.request.user.username})
            return HttpResponseRedirect(url)
        else:
            return super(UserGroupList, self).get(request, *args, **kwargs)


class UserGroupCreate(LoginRequiredMixin, CreateView):
    model = UserGroup
    fields = ['title', 'description']
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'
    template_name = 'users/usergroup_form.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        try:
            self.object.save()
        except ValidationError:
            messages.add_message(self.request, messages.ERROR, _("Already exists a user group with that name"))
            return render(self.request, self.template_name, {'form': form})
        url = reverse("usergroup_detail", kwargs={'uuid': self.object.uuid, 'username': self.request.user.username})

        return HttpResponseRedirect(url)


class UserGroupDetail(LoginRequiredMixin, DetailView):
    model = UserGroup
    template_name = 'users/usergroup_details.html'
    add_workspace_to_group_template = 'users/add_workspace_to_group.html'
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'

    def get_context_data(self, **kwargs):

        context = super(UserGroupDetail, self).get_context_data(**kwargs)

        context['workspace_permissions_list'] = WorkspacePermissions.objects.filter(group=self.get_object(),
                                                                                    group__owner=self.request.user)
        context['user_list'] = self.get_object().users.all
        context['WORKSPACE_PERMISSIONS'] = SELECTABLE_WORKSPACE_PERMISSIONS

        return context

    def get(self, request, *args, **kwargs):

        usergroup_uuid = kwargs.pop('uuid', None)
        username = kwargs.pop('username', None)

        try:

            self.object = self.get_object()
            if username != self.request.user.username or not valid_uuid(
                    usergroup_uuid) or usergroup_uuid != str(self.object.uuid):
                raise Http404

            if request.user.pk is not self.object.owner.pk:
                raise Http404
        except Http404:
            url = reverse('goals_list', kwargs={'username': self.request.user.username})
            return HttpResponseRedirect(url)

        context = self.get_context_data(object=self.object)

        if self.get_object() is not None:
            return self.render_to_response(context)
        else:
            url = reverse('goals_list', kwargs={'username': self.request.user.username})
            return HttpResponseRedirect(url)

    def remove_user_from_usergroup(self, request):
        user_remove_id = request.POST.get('user_remove_id', None)

        if valid_uuid(user_remove_id):
            try:
                user = MyUser.objects.get(uuid=user_remove_id)
            except MyUser.DoesNotExist:
                user = None

            if user is not None:
                #logger.debug("is not none")
                self.get_object().users.remove(user)
                return True
        return None

    def remove_workspace_from_usergroup(self, request):
        workspace_remove_id = request.POST.get('workspace_remove_id', None)

        if valid_uuid(workspace_remove_id):
            try:
                workspace_permission = WorkspacePermissions.objects.get(group=self.get_object(),
                                                                        group__owner=self.request.user,
                                                                        uuid=workspace_remove_id)
            except WorkspacePermissions.DoesNotExist:
                logger.debug(self.__class__.__name__ + ":Workspace desnt exist")
                workspace_permission = None

        if workspace_permission is not None:
            logger.debug(self.__class__.__name__ + ":Workspace permission is not none")
            workspace_permission.delete()
            return True
        return None

    def post(self, request, *args, **kwargs):

        url = reverse("usergroup_detail",
                      kwargs={'uuid': self.get_object().uuid, 'username': self.request.user.username})

        if 'workspace_remove_id' in request.POST:

            removed = self.remove_workspace_from_usergroup(request)
            if removed:
                messages.add_message(request, messages.SUCCESS, _("Workspace was removed successfully"))
            else:
                messages.add_message(request, messages.ERROR, _("Workspace can not be removed"))

        if 'user_remove_id' in request.POST:

            removed = self.remove_user_from_usergroup(request)
            if removed:
                messages.add_message(request, messages.SUCCESS, _("User was removed successfully"))
            else:
                messages.add_message(request, messages.ERROR, _("User can not be removed"))

        return HttpResponseRedirect(url)


class UserGroupUpdate(LoginRequiredMixin, UpdateView):
    model = UserGroup
    template_name = "users/usergroup_update_form.html"
    fields = ['title', 'description']
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'

    def get_success_url(self):
        url = reverse("usergroup_detail",
                      kwargs={'uuid': self.get_object().uuid, 'username': self.request.user.username})
        return url


class UserGroupDelete(LoginRequiredMixin, DeleteView):
    model = UserGroup
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'

    def get_success_url(self):
        return reverse("usergroups_list", kwargs={'username': self.request.user.username})

    def get_object(self, queryset=None):

        uuid = self.kwargs.get(self.slug_url_kwarg, None)

        if not valid_uuid(uuid):
            raise Http404

        obj = super(UserGroupDelete, self).get_object()
        if not obj.owner == self.request.user:
            raise Http404
        messages.add_message(self.request, messages.SUCCESS, _("User group deleted successfully"))
        return obj


class AddUserToGroupView(LoginRequiredMixin, TemplateView):
    form_class = AddUserToGroupForm
    template_name = 'users/add_user_to_group_form.html'

    def user_group_to_add_is_valid(self, request, user_group, **kwargs):
        group = UserGroup.objects.get(uuid=user_group, owner=self.request.user)
        if group is None:
            return None
        return group

    def user_exists(self, username):
        try:
            MyUser.objects.get(username=username)
            logger.debug("user exist "+username)
        except MyUser.DoesNotExist:
            logger.debug("user does not exist " + username)
            return False

        return True

    def group_has_user(self, request, usergroup, username):
        if self.user_exists(username):
            return False
        if username in usergroup.users.all():
            messages.add_message(request, messages.ERROR, _("An user can be added only once to a group"))
            return True
        return False

    def get(self, request, *args, **kwargs):
        if 'uuid' in kwargs:
            user_group_add = kwargs.get('uuid', None)

            if valid_uuid(user_group_add):
                if user_group_add is not None:
                    group_to_add = self.user_group_to_add_is_valid(request, user_group_add)
                    if group_to_add:
                        form = AddUserToGroupForm(user=self.request.user, initial={
                            'usergroup': group_to_add.uuid
                        })
                        return render(request, self.template_name, {'object': group_to_add, 'form': form})
        return HttpResponseRedirect('/')

    def selected_user_is_owner(self, form, request):
        if form.cleaned_data['user'] == self.request.user.username:
            error_message = _("You can't add yourself to one your groups")
            messages.add_message(request, messages.ERROR, error_message)
            return True
        return False

    def post(self, request, *args, **kwargs):

        usergroup_uuid = request.POST.get('usergroup', None)

        if not valid_uuid(usergroup_uuid):
            messages.add_message(request, messages.ERROR, "Invalid data")

            url = reverse("usergroup_detail",
                          kwargs={'uuid': usergroup_uuid, 'username': self.request.user.username})

            return HttpResponseRedirect(url)

        form = self.form_class(self.request.user, request.POST)

        if form.is_valid():
            logger.debug("form is valid!")
            url = reverse("usergroup_detail", kwargs={'uuid': form.cleaned_data['usergroup'],
                                                      'username': self.request.user.username})

            user_is_owner = self.selected_user_is_owner(form, request)
            group_to_add = self.user_group_to_add_is_valid(request, form.cleaned_data['usergroup'])

            if not self.user_exists(form.cleaned_data['user']):
                messages.add_message(request, messages.ERROR, _("User does not exist"))
                return render(request, self.template_name, {
                    'form': form,
                    'object': group_to_add})

            try:
                if user_is_owner or not group_to_add or self.group_has_user(request, group_to_add,
                                                                            form.cleaned_data['user']):
                    raise IntegrityError
                else:
                    self.save(form, request)
                    messages.add_message(request, messages.SUCCESS, _("User was added to group successfully"))

            except IntegrityError:
                logger.debug("Integrity Error")
                return render(request, self.template_name, {
                    'form': form,
                    'object': group_to_add})

            return HttpResponseRedirect(url)

        else:

            logger.debug("is not valid")
            group_to_add = self.user_group_to_add_is_valid(request, form.cleaned_data['usergroup'])
            if group_to_add:
                return render(request, self.template_name, {'form': form, 'object': group_to_add})

        return HttpResponseRedirect('/')

    def save(self, form, request):
        #logger.debug("save")

        #logger.debug("Group:" + form.cleaned_data['usergroup'])
        group_to_add = self.user_group_to_add_is_valid(request, form.cleaned_data['usergroup'])

        user = form.cleaned_data['user']

        if group_to_add is None:
            url = reverse('goals_list', kwargs={'username': self.request.user.username})
            return HttpResponseRedirect(url)

        if user in group_to_add.users.all():
            raise IntegrityError

        group_to_add.users.add(MyUser.objects.get(username=user))
        group_to_add.save()

        return


class Profile(LoginRequiredMixin, TemplateView):
    template_name = 'users/user_profile.html'

    def get(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect('/')

        form = AvatarChangeForm(instance=request.user)

        return render(request, self.template_name, {'form': form})

    def post(self, request):
        if request.method == 'POST':
            #logger.debug(request.POST)
            if 'avatar' in request.FILES:
                form = AvatarChangeForm(request.POST, request.FILES, instance=request.user)
                if form.is_valid():
                    logger.debug("Avatar form IS valid")
                    form.save()
                    return HttpResponseRedirect(reverse('profile'))
                else:
                    logger.debug("Avatar form is NOT valid")

            form = AvatarChangeForm(instance=request.user)

        return render(request, self.template_name, {'form': form})
