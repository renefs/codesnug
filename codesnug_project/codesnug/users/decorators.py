__author__ = 'rene'
from django_ajax.decorators import ajax
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from dateutil import parser

from codesnug.goals.models import WorkspacePermissions
from codesnug.settings import SELECTABLE_WORKSPACE_PERMISSIONS

@ajax
@login_required
def update_user_first_name(request):
    if request.user.is_authenticated():
        if 'new_name' in request.POST:

            request.user.first_name = request.POST.get('new_name', "")

            if request.user.first_name == "":
                return {'id': 'null'}
            try:
                request.user.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': request.user.first_name}

@ajax
@login_required
def update_user_last_name(request):
    if request.user.is_authenticated():
        if 'new_name' in request.POST:

            request.user.last_name = request.POST.get('new_name', "")

            if request.user.last_name == "":
                return {'id': 'null'}
            try:
                request.user.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': request.user.last_name}

@ajax
@login_required
def update_user_email(request):
    if request.user.is_authenticated():
        if 'new_name' in request.POST and 'verification' in request.POST:

            email1 = request.POST.get('new_name', "").lower().strip()
            email2 = request.POST.get('verification', "").lower().strip()

            if email1 != email2:
                return {'id': 'null'}

            try:
                validate_email(email1)
            except ValidationError:
                return {'id': 'null'}

            if User.objects.filter(email=email1):
                return {'id': 'null'}

            if email1 == "":
                return {'id': 'null'}

            request.user.email = email1

            try:
                request.user.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': request.user.email}

@ajax
@login_required
def update_user_birth_date(request):
    if request.user.is_authenticated():
        if 'new_name' in request.POST:
            print request.POST.get('new_name', "")

            request.user.date_of_birth = parser.parse(request.POST.get('new_name', ""))

            if request.user.date_of_birth == "":
                return {'id': 'null'}
            try:
                request.user.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': str(request.user.date_of_birth)}

@ajax
@login_required
def update_user_password(request):
    if request.user.is_authenticated():
        if 'new_name' in request.POST and 'verification' in request.POST and 'old' in request.POST:

            password1 = request.POST.get('new_name', "")
            password2 = request.POST.get('verification', "")

            old = request.POST.get('old', "")

            if password1 != password2:
                return {'id': 'null'}

            if password1 == "":
                return {'id': 'null'}

            if not request.user.check_password(old):
                return {'id': 'null'}

            request.user.set_password(password1)

            try:
                request.user.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': '*'*len(password1)}


@ajax
@login_required
def update_workspace_permission(request):
    print request.POST
    if request.user.is_authenticated():
        if 'new_permission' in request.POST and 'workspace' in request.POST:

            new_permission = request.POST.get('new_permission', "")
            workspace = request.POST.get('workspace', "")
            usergroup = request.POST.get('group', "")

            if new_permission == "" or workspace == "" or usergroup == "":
                return {'id': 'null'}
            try:
                if int(new_permission) not in dict(SELECTABLE_WORKSPACE_PERMISSIONS):
                    return {'id': 'null'}

                workspace_permission = WorkspacePermissions.objects.get(group__uuid=usergroup,
                                                    group__owner=request.user, workspace__uuid=workspace)

                workspace_permission.permission = int(new_permission)
                workspace_permission.save()

            except IntegrityError:
                return {'id': 'null'}

            return {'title': new_permission}