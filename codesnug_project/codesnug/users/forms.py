# -*- coding: utf-8 -*-

__author__ = 'rene'

from registration.forms import RegistrationFormUniqueEmail
from django import forms
from codesnug import settings
from django.utils.translation import ugettext_lazy as _
#from django.contrib.auth.models import User
from codesnug.users.models import MyUser
from codesnug.users.lookups import MyUserLookup
from selectable.forms import AutoCompleteWidget
from awesome_avatar import forms as avatar_forms


class UserRegistrationForm(RegistrationFormUniqueEmail):
    #user
    first_name = forms.CharField(required=False, max_length=30)
    last_name = forms.CharField(required=False, max_length=100)
    email = forms.EmailField(required=True, label='Your e-mail address')
    avatar = avatar_forms.AvatarField(required=False, disable_preview=True, width=200, height=200)

    #profile
    date_of_birth = forms.DateField(required=True, input_formats=settings.DATE_INPUT_FORMATS)

    #form
    tos = forms.BooleanField(widget=forms.CheckboxInput,
                             label=_(u'I have read and agree to the Terms of Service'),
                             error_messages={'required': _("You must agree to the terms to register")})

    verify = forms.CharField(required=True, max_length=100)

    def clean_verify(self):
        if self.cleaned_data['verify'] != 'GRACIASPORLAINVITACION':
            raise forms.ValidationError(_("Currently we are in beta phase and you must have an invitation to join."))
        return self.cleaned_data['verify']


class AddUserToGroupForm(forms.Form):
    user = forms.CharField(
        label='Type the name of a user (AutoCompleteWidget)',
        widget=AutoCompleteWidget(MyUserLookup),
        required=True,
        )
    usergroup = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, user, *args, **kwargs):
        super(AddUserToGroupForm, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = MyUser.objects.all().exclude(pk=user.pk)


class AvatarChangeForm(forms.ModelForm):

    class Meta:
        model = MyUser
        fields = ['avatar']