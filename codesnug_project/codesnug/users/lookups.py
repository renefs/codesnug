__author__ = 'rene'

from selectable.base import ModelLookup
from selectable.registry import registry
from codesnug.users.models import MyUser
from selectable.decorators import login_required

class MyUserLookup(ModelLookup):
    model = MyUser
    search_fields = ('username__icontains', )

    filters = {'is_active': True, 'is_superuser': False }

    def get_item_value(self, item):
        # Display for currently selected item
        return item.username

    def get_item_label(self, item):
        # Display for choice listings
        return u'%s, %s' % (item.avatar.url, item.username)

MyUserLookup = login_required(MyUserLookup)

registry.register(MyUserLookup)