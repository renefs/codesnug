__author__ = 'rene'

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import transaction
from django.test import TestCase
from django.db import IntegrityError
from codesnug.users.models import MyUser, UserGroup
from django.utils import timezone


# Create your tests here.
class MyUserModelTestCase(TestCase):
    def test_add_user(self):
        MyUser.objects.create_user('user1', 'prueba@prueba.com', '1111')
        MyUser.objects.create_user('user2', 'prueba2@prueba.com', '1111')

        self.assertEqual(MyUser.objects.all().count(), 2)

    def test_add_user_with_error(self):
        MyUser.objects.create_user('user1', 'prueba@prueba.com', '1111')

        try:
            with transaction.atomic():
                MyUser.objects.create_user('user2', 'prueba@prueba.com', '1111')
                self.assertTrue(True, 'Duplicated email allowed')
        except IntegrityError:
            pass

        try:
            with transaction.atomic():
                MyUser.objects.create_user('user1', 'prueba2@prueba.com', '1111')
                self.assertTrue(True, 'Duplicated username allowed')
        except IntegrityError:
            pass

        self.assertEqual(MyUser.objects.all().count(), 1)

    def test_remove_user(self):
        user = MyUser.objects.create_user('user1', 'prueba@prueba.com', '1111')
        user2 = MyUser.objects.create_user('user2', 'prueba2@prueba.com', '1111')

        self.assertEqual(MyUser.objects.all().count(), 2)
        user.delete()

        self.assertEqual(MyUser.objects.all().count(), 1)

        user2.delete()
        self.assertEqual(MyUser.objects.all().count(), 0)

    def test_check_user_data(self):

        username = 'user1'
        email = 'prueba@prueba.com'
        password = '1111'
        first_name = '^WPKO^?OPDPKL'
        last_name = '39249idfijdjfhsdjfhs'

        MyUser.objects.create_user(username, email, password)
        user1 = MyUser.objects.get(username=username)
        self.assertTrue(user1)

        self.assertTrue(user1.username == username)
        self.assertTrue(user1.email == email)

        user1.first_name = first_name
        self.assertTrue(user1.first_name == first_name)

        user1.last_name = last_name
        self.assertTrue(user1.last_name == last_name)

        self.assertTrue(user1.is_staff is False)
        self.assertTrue(user1.is_active is True)
        self.assertTrue(user1.date_joined < timezone.now())


class UserGroupTestCase(TestCase):
    user = None
    user2 = None

    title = 'sdklfjoi3jrf3jfow3o'
    description = 'skldjfklsjdkfi3023ijfdksjfsd'

    title2 = 'sdklfjoi3jrf3jfow3o2'

    def setUp(self):
        self.user = MyUser.objects.create_user('user1', 'prueba@prueba.com', '1111')
        self.user2 = MyUser.objects.create_user('user2', 'prueba2@prueba2.com', '1111')

    def tearDown(self):
        self.user = None
        self.user2 = None

    def test_add_user_group(self):

        # group1
        group1 = UserGroup.objects.create_user_group(self.title, self.user, self.description)
        self.assertTrue(group1)
        group1.save()

        self.assertEqual(UserGroup.objects.all().count(), 1)

        found_group = UserGroup.objects.get(title=self.title, owner=self.user, description=self.description)
        self.assertTrue(found_group)

        #group2
        group2 = UserGroup.objects.create_user_group(self.title2, self.user)
        self.assertTrue(group2)
        group2.save()

        self.assertEqual(UserGroup.objects.all().count(), 2)

        try:
            UserGroup.objects.get(title=self.title2, owner=self.user, description=self.description)
            self.assertTrue(True, 'Found incorrect user group')
        except ObjectDoesNotExist:
            pass

        found_group = UserGroup.objects.get(title=self.title2, owner=self.user, description='')
        self.assertTrue(found_group)

    def test_add_same_group_twice(self):

        # Add the group
        group1 = UserGroup.objects.create_user_group(self.title, self.user, self.description)
        self.assertTrue(group1)
        group1.save()
        self.assertEqual(UserGroup.objects.all().count(), 1)

        #Add the group
        try:
            with transaction.atomic():
                group2 = UserGroup.objects.create_user_group(self.title, self.user, self.description)
            self.assertTrue(True, 'Duplicated title allowed')
        except ValidationError:
            pass
        self.assertEqual(UserGroup.objects.all().count(), 1)

    def test_add_same_group_to_different_users(self):

        # Add the group
        group1 = UserGroup.objects.create_user_group(self.title, self.user, self.description)
        self.assertTrue(group1)
        self.assertEqual(UserGroup.objects.all().count(), 1)

        #Add the group
        group2 = UserGroup.objects.create_user_group(self.title, self.user2, self.description)
        self.assertTrue(group2)
        self.assertEqual(UserGroup.objects.all().count(), 2)

        self.assertEqual(self.user.owned_groups.all().count(), 1)
        self.assertEqual(self.user2.owned_groups.all().count(), 1)

    def test_remove_user_group(self):

        # Add the group
        group1 = UserGroup.objects.create_user_group(self.title, self.user, self.description)
        self.assertTrue(group1)
        group1.save()
        self.assertEqual(UserGroup.objects.all().count(), 1)

        #Find and remove the group
        found_group = UserGroup.objects.get(title=self.title, owner=self.user, description=self.description)
        found_group.delete()
        self.assertEqual(UserGroup.objects.all().count(), 0)

    def test_user_has_group(self):
        # Add the group
        group1 = UserGroup.objects.create_user_group(self.title, self.user, self.description)
        self.assertTrue(group1)
        group1.save()
        self.assertEqual(UserGroup.objects.all().count(), 1)

        self.assertEqual(self.user.owned_groups.all().count(), 1)
        self.assertEqual(self.user.owned_groups.all()[0], group1)