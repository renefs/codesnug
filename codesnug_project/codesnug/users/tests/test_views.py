from django.core.exceptions import ValidationError
from django.db import DataError, transaction
from codesnug.goals.models import Workspace

__author__ = 'rene'

from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from codesnug.users.models import MyUser, UserGroup


#Hack. Reverse not working ok for redirects? WHY? WHYYYYYYYY?
def build_test_url(url):
    return 'https://testserver:80'+url


class UserViewsTestCase(TestCase):
    client = None

    username = 'rene'
    password = 'rene'
    password2 = 'rene'
    date_of_birth = 'rene'

    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_home(self):
        #Simple get
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_home_register(self):
        #Correct post to home register form. Must redirect.
        response = self.client.post(reverse('home'),
                                    {'username': 'rene',
                                     'password1': 'rene',
                                     'password2': 'rene',
                                     'date_of_birth': '11/02/1984',
                                     'email': 'test@test.com',
                                     'tos': 'yes'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, build_test_url(reverse('registration_complete')))

    def test_login(self):
        #Login with a user that does not exist display login page
        response = self.client.post(reverse('auth_login'),
                                    {'username': 'rene',
                                     'password': 'rene',
                                     })
        self.assertEqual(response.status_code, 200)

        #We create the user on the home iwth valid data
        response = self.client.post(reverse('home'),
                                    {'username': 'rene',
                                     'password1': 'rene',
                                     'password2': 'rene',
                                     'date_of_birth': '11/02/1984',
                                     'email': 'test@test.com',
                                     'tos': 'yes'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, build_test_url(reverse('registration_complete')))

        self.assertEqual(MyUser.objects.all().count(), 1)
        new_user = MyUser.objects.get(username='rene')
        self.failIf(new_user.is_active)
        new_user.is_active = True
        new_user.save()

        #We do the login again. Must redirect to Goals
        #Login with a user that exists should redirect to home
        response = self.client.post(reverse('auth_login'),
                                    {'username': 'rene',
                                     'password': 'rene',
                                     }, follow=True)
        #With follow, the status_code will be 200. It should land on the goals list page
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, build_test_url(reverse('goals_list', kwargs={'username': new_user.username})))

    def test_home_register_error(self):
        response = self.client.post(reverse('home'), {'username': 'rene',
                                                      'password1': 'rene',
                                                      'password2': 'rene2',
                                                      'date_of_birth': '11/02/1984',
                                                      'email': 'test@test.com',
                                                      'tos': 'yes'})

        self.assertEqual(response.status_code, 200)


class UsergroupCrudTest(TestCase):

    client = None
    new_user = None
    new_user2 = None
    new_user3 = None

    workspace_user = None
    usergroup_user_title = 'Usergroup User'
    workspace_user_title = 'workspace_user'

    username = 'rene'
    password = 'rene'
    password2 = 'rene'
    date_of_birth = 'rene'

    subscriber = 2

    #Hack. Reverse not working ok for redirects? WHY? WHYYYYYYYY?
    def build_test_url(self, url):
        return 'https://testserver:80'+url

    def create_users(self):

        #We create the user on the home iwth valid data
        self.client.post(reverse('home'),
                         {'username': 'rene',
                          'password1': 'rene',
                          'password2': 'rene',
                          'date_of_birth': '11/02/1984',
                          'email': 'test@test.com',
                          'tos': 'yes'})


        self.new_user = MyUser.objects.get(username='rene')
        self.new_user.is_active = True
        self.new_user.save()

        #We create the user on the home iwth valid data
        self.client.post(reverse('home'),
                         {'username': 'rene1',
                          'password1': 'rene1',
                          'password2': 'rene1',
                          'date_of_birth': '11/02/1984',
                          'email': 'test1@test.com',
                          'tos': 'yes'})


        self.new_user2 = MyUser.objects.get(username='rene1')
        self.new_user2.is_active = True
        self.new_user2.save()

        #We create the user on the home iwth valid data
        self.client.post(reverse('home'),
                         {'username': 'rene2',
                          'password1': 'rene2',
                          'password2': 'rene2',
                          'date_of_birth': '11/02/1984',
                          'email': 'test2@test.com',
                          'tos': 'yes'})


        self.new_user3 = MyUser.objects.get(username='rene2')
        self.new_user3.is_active = True
        self.new_user3.save()

        #Login with a user that exists should redirect to home
        self.client.post(reverse('auth_login'),
                         {'username': self.username,
                          'password': self.password,
                          }, follow=True)

    def create_workspaces(self):
        #Login with a user
        self.client.post(reverse('auth_login'),
                         {'username': self.new_user.username,
                          'password': self.new_user.username,
                          }, follow=True)

        #adding workspace
        self.client.post(reverse('workspaces_add'),
                         {'title': self.workspace_user_title,
                          'description': 'Workspace description for User',
                          }, follow=True)

        self.workspace_user = Workspace.objects.get(title=self.workspace_user_title)

    def setUp(self):
        # Every test needs a client.
        self.client = Client()
        self.create_users()
        self.create_workspaces()

    def test_add_workspace_to_group(self):

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': self.usergroup_user_title})

        new_group = UserGroup.objects.get(title=self.usergroup_user_title)

        response = self.client.post(reverse('asign_workspace_group',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'usergroup': new_group.uuid,
                                                'workspace': self.workspace_user.pk,
                                                'permission': self.subscriber}, follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        self.assertContains(response, 'alert-danger', 0, 200)
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertEqual(new_group.usergroups_permissions.all().count(), 1)

    def test_remove_workspace_from_group(self):

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': self.usergroup_user_title})

        new_group = UserGroup.objects.get(title=self.usergroup_user_title)

        response = self.client.post(reverse('asign_workspace_group',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'usergroup': new_group.uuid,
                                                'workspace': self.workspace_user.pk,
                                                'permission': self.subscriber}, follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        self.assertContains(response, 'alert-danger', 0, 200)
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertEqual(new_group.usergroups_permissions.all().count(), 1)

        response = self.client.post(reverse('usergroup_detail',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'workspace_remove_id': new_group.usergroups_permissions.all()[0].uuid,
                                               }, follow=True)
        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        self.assertContains(response, 'alert-danger', 0, 200)
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertEqual(new_group.usergroups_permissions.all().count(), 0)

    def test_add_user_to_group(self):

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': self.usergroup_user_title})

        new_group = UserGroup.objects.get(title=self.usergroup_user_title)

        response = self.client.post(reverse('asign_user_group',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'usergroup': new_group.uuid, 'user': self.new_user2.username}, follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        self.assertContains(response, 'alert-danger', 0, 200)
        self.assertContains(response, 'alert-success', 1, 200)

        self.assertEqual(new_group.users.all().count(), 1)

    def test_remove_user_from_group(self):

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': self.usergroup_user_title})

        new_group = UserGroup.objects.get(title=self.usergroup_user_title)

        response = self.client.post(reverse('asign_user_group',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'usergroup': new_group.uuid, 'user': self.new_user2.username}, follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        self.assertContains(response, 'alert-danger', 0, 200)
        self.assertContains(response, 'alert-success', 1, 200)

        self.assertEqual(new_group.users.all().count(), 1)

        response = self.client.post(reverse('usergroup_detail',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'user_remove_id': self.new_user2.uuid,
                                                }, follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        self.assertContains(response, 'alert-danger', 0, 200)
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertEqual(new_group.users.all().count(), 0)

    def test_get_usergroup_list(self):
        #Simple get
        response = self.client.get(reverse('usergroups_list', kwargs={'username': self.new_user.username}))
        self.assertEqual(response.status_code, 200)

    def test_usergroup_add(self):

        group_title = 'test_group'

        group_title2 = 'test_group2'
        group_description2 = 'This is the description for group 2'

        #Simple get
        response = self.client.get(reverse('usergroups_add'))
        self.assertEqual(response.status_code, 200)

        #Invalid post data
        response = self.client.post(reverse('usergroups_add'), {})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'has-error', 1, 200)

        #Valid post data (only title)
        response = self.client.post(reverse('usergroups_add'), {'title': group_title})

        new_group = UserGroup.objects.get(title=group_title)

        self.assertTrue(new_group)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        #Add group with existing title
        try:
            response = self.client.post(reverse('usergroups_add'), {'title': group_title})
            self.assertTrue(True, 'Allowed usergroup with same title')
        except ValidationError:
            pass
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger', 1, 200)

        #Add group with title and description
        #Valid post data with title and description
        response = self.client.post(reverse('usergroups_add'), {'title': group_title2, 'description': group_description2})

        new_group2 = UserGroup.objects.get(title=group_title2, description=group_description2)

        self.assertTrue(new_group2)
        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group2.uuid})))

    def test_usergroup_remove_success(self):

        group_title = 'test_group'

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': group_title})
        new_group = UserGroup.objects.get(title=group_title)

        response = self.client.post(reverse('usergroup_delete',
                                    kwargs={'username': self.new_user.username,
                                            'uuid': new_group.uuid},
                                    ), follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroups_list',
                                                              kwargs={'username': self.new_user.username})))
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertEqual(UserGroup.objects.all().count(), 0)

    def test_usergroup_remove_error(self):

        group_title = 'test_group'

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': group_title})
        new_group = UserGroup.objects.get(title=group_title)

        response = self.client.post(reverse('usergroup_delete',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': 'dsfsf'},
                                            ), follow=True)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(UserGroup.objects.all().count(), 1)

    def test_usergroup_update(self):

        group_title = 'test_group'

        group_new_title = 'new_test_group'
        group_new_description = 'New description of the group'

        #Valid post data (only title)
        self.client.post(reverse('usergroups_add'), {'title': group_title})
        new_group = UserGroup.objects.get(title=group_title)

        #Test update group info
        response = self.client.post(reverse('usergroup_update',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': new_group.uuid},
                                            ), {'title': group_new_title,
                                                'description': group_new_description}, follow=True)

        self.assertRedirects(response, build_test_url(reverse('usergroup_detail',
                                                              kwargs={'username': self.new_user.username,
                                                                      'uuid': new_group.uuid})))

        #Check updated values
        new_group = UserGroup.objects.get(uuid=new_group.uuid)

        self.assertEqual(group_new_title, new_group.title)
        self.assertEqual(group_new_description, new_group.description)