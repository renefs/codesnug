# -*- coding: utf-8 -*-
import logging
from django.core.exceptions import ValidationError
from django.utils.decorators import method_decorator
from registration.signals import user_registered
from dateutil import parser

from uuidfield import UUIDField
from codesnug.settings import MAX_UPLOAD_SIZE
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, UserManager, PermissionsMixin

import os
import re
from awesome_avatar.settings import config
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from awesome_avatar.fields import AvatarField
from codesnug.users.storage import OverwriteStorage
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core import validators
from django.core.mail import send_mail
from django.utils.http import urlquote

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

try:
    from PIL import Image
except ImportError:
    from PIL import Image

logger = logging.getLogger(__name__)


def get_image_path(instance, filename):
    return os.path.join('avatars', str(instance.username), filename)


class MyUser(AbstractBaseUser, PermissionsMixin):

    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, numbers and '
                                            '@/./+/-/_ characters'),
                                validators=[
                                    validators.RegexValidator(re.compile('^[\w.@+-]+$'),
                                                              _('Enter a valid username.'), 'invalid')
                                ])
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=False, unique=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    date_of_birth = models.DateField(null=True)
    uuid = UUIDField(auto=True)
    avatar = AvatarField(upload_to=get_image_path,
                         storage=OverwriteStorage(),
                         width=200,
                         height=200,
                         default='avatars/default.jpg')

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return self.username

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


class UserGroupManager(models.Manager):
    def create_user_group(self, title, owner, description=''):

        if title is None or title is '':
            raise ValidationError('Usergroup title must be set')
        if owner is None:
            raise ValidationError('Owner must be set')

        usergroup = self.create(title=title, owner=owner)
        usergroup.description = description
        return usergroup


class UserGroup(models.Model):
    title = models.CharField(max_length=200)
    owner = models.ForeignKey(MyUser, related_name='owned_groups')
    description = models.TextField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    users = models.ManyToManyField(MyUser, blank=True, null=True)
    uuid = UUIDField(auto=True)

    objects = UserGroupManager()

    def save(self, *args, **kwargs):
        self.full_clean()
        super(UserGroup, self).save(*args, **kwargs)

    class Meta:
        unique_together = (("title", "owner"),)
        ordering = ('created_at', )


def user_registered_callback(sender, user, request, **kwargs):

    if 'first_name' in request.POST:
        user.first_name = request.POST.get('first_name', None)
    if 'last_name' in request.POST:
        user.last_name = request.POST.get('last_name', None)

    user.date_of_birth = parser.parse(request.POST.get('date_of_birth', None))

    user.save()

    if 'avatar' in request.FILES:

        avatar_file = request.FILES['avatar']
        if avatar_file.size > int(MAX_UPLOAD_SIZE):
            return

        x1 = request.POST.get('avatar-x1', 0)
        y1 = request.POST.get('avatar-y1', 0)
        x2 = request.POST.get('avatar-x2', x1)
        y2 = request.POST.get('avatar-y2', y1)
        ratio = request.POST.get('avatar-ratio', 1)
        ratio = float(1 if not ratio else ratio)

        box_raw = [x1, y1, x2, y2]
        box = []

        for coord in box_raw:
            try:
                coord = int(coord)
            except ValueError:
                coord = 0

            if ratio > 1:
                coord = int(coord * ratio)
            box.append(coord)

        image = Image.open(StringIO(avatar_file.read()))
        image = image.crop(box)
        if not getattr(config, 'no_resize', False):
            image = image.resize((config.width, config.height), Image.ANTIALIAS)

        content = StringIO()
        image.save(content, config.save_format, quality=config.save_quality)

        file_name = u'{}.{}'.format(os.path.splitext(avatar_file.name)[0], config.save_format)

        # new_data = SimpleUploadedFile(file.name, content.getvalue(), content_type='image/' + config.save_format)
        new_data = InMemoryUploadedFile(content, None, 'avatar.'+config.save_format, 'image/' + config.save_format, len(content.getvalue()), None)

        user.avatar = new_data

        user.save()

user_registered.connect(user_registered_callback)

