# -*- coding: utf-8 -*-
__author__ = 'rene'
from django.shortcuts import render, HttpResponseRedirect
from registration.views import RegistrationView
from django.contrib.sites.models import RequestSite, Site
from registration.models import RegistrationProfile
from registration import signals
from django.core.urlresolvers import reverse

from django.contrib import messages
from django.core.mail import EmailMessage
from django.utils.translation import ugettext as _

from codesnug.users.forms import UserRegistrationForm
from django.views.generic import TemplateView, FormView
from codesnug.forms import ContactForm
from codesnug.settings import DEFAULT_FROM_EMAIL, DEFAULT_TO_EMAIL


class Home(RegistrationView):

    template_name = 'home.html'
    success_url = '/accounts/register/complete/'
    form_class = UserRegistrationForm

    def get(self, request, **kwargs):
        if request.user.is_authenticated():
            url = reverse('goals_list', kwargs={'username': request.user.username})
            return HttpResponseRedirect(url)
        form = UserRegistrationForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            self.register(request, **form.cleaned_data)

            return HttpResponseRedirect(self.success_url)

        return render(request, self.template_name, {'form': form})

    def register(self, request, **cleaned_data):

        username, email, password = cleaned_data['username'], cleaned_data['email'], cleaned_data['password1']
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        new_user = RegistrationProfile.objects.create_inactive_user(username, email, password, site)
        signals.user_registered.send(sender=self.__class__, user=new_user, request=request)

        return new_user


class TermsConditions(TemplateView):
    template_name = 'terms_conditions.html'


class Faqs(TemplateView):
    template_name = 'faqs.html'


class Api(TemplateView):
    template_name = 'api.html'


class TheProject(TemplateView):
    template_name = 'the_project.html'


class Privacy(TemplateView):
    template_name = 'privacy.html'


class ContactUs(FormView):
    template_name = 'contact_us.html'
    form_class = ContactForm

    def get_success_url(self):
        return reverse('contact_us')

    def post(self, request, *args, **kwargs):
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data

            html_content = '<p>Nombre: <strong>' + cd['name'] + '</strong><p><strong>Email del remitente:</strong></p><p>' + \
                           cd['email'] + '</p><p><strong>Cuerpo:</strong></p>' + cd['message'] + '</p>'

            msg = EmailMessage('[Codesnug] Formulario de contacto', html_content, DEFAULT_FROM_EMAIL, [DEFAULT_TO_EMAIL])
            msg.content_subtype = "html"  # Main content is now text/html

            messages.add_message(request, messages.SUCCESS,  _(u'Message was successfully sent. ¡Thank you!'))
            return HttpResponseRedirect(self.get_success_url())
        return render(request, self.template_name, {'form': form})


class WhoWeAre(TemplateView):
    template_name = 'who_we_are.html'