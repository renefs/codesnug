# -*- coding: utf-8 -*-
from django.contrib import messages
from codesnug.search.forms import GoalSearchForm
from codesnug.utils import valid_uuid

__author__ = 'rene'


from django.views.generic import ListView, CreateView, DetailView, UpdateView, TemplateView, DeleteView
from django.shortcuts import HttpResponseRedirect, HttpResponse, get_object_or_404, render
from django.core.urlresolvers import reverse

from django.shortcuts import render
import logging
from django.http import Http404
from django.utils.translation import ugettext_lazy as _
from braces.views import LoginRequiredMixin

from codesnug.settings import WORKSPACE_PERMISSIONS, VERSIONS_LIMIT
from codesnug.goals.forms import GoalForm
from codesnug.goals.models import Workspace, WorkspacePermissions, Goal, Snippet, Version
from codesnug.users.models import MyUser

logger = logging.getLogger(__name__)


class GoalCreate(LoginRequiredMixin, CreateView):
    model = Goal
    form_class = GoalForm
    template_name = 'goals/goal_form.html'

    def get_success_url(self):
        return reverse("goals_list", kwargs={'username': self.request.user.username})

    def are_workspaces_from_owner(self, workspaces):
        for workspace in workspaces:
            if workspace.owner != self.object.owner:
                return None
        return True

    def are_tags_from_owner(self, tags):
        for tag in tags:
            if tag.owner != self.object.owner:
                return None
        return True

    def process_valid_form(self, form):

        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()

        if self.are_workspaces_from_owner(form.cleaned_data['workspaces']) and form.cleaned_data['workspaces'] != '':
            self.object.workspaces = form.cleaned_data['workspaces']

        if self.are_tags_from_owner(form.cleaned_data['tags']) and form.cleaned_data['tags'] != '':
            self.object.tags = form.cleaned_data['tags']

        for i in range(int(form.cleaned_data['snippet_count'])):
            new_snippet = Snippet()
            new_snippet.goal = self.object
            new_snippet.author = self.request.user
            new_snippet.title = form.cleaned_data['snippet_title_' + str(i)]
            new_snippet.language = form.cleaned_data['snippet_language_' + str(i)]
            new_snippet.text = form.cleaned_data['snippet_text_' + str(i)]
            new_snippet.save()

        self.object.save()

        return self.object

    def get_absolute_url(self):
        return self.success_url

    def get(self, request, *args, **kwargs):
        form = GoalForm(user=request.user)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):

        snippet_count = request.POST.get('snippet_count', None)

        try:
            int(snippet_count)
        except ValueError:
            raise Http404

        form = GoalForm(request.POST, snippet_count=snippet_count, user=request.user)
        if form.is_valid():
            print "is_valid"
            self.object = self.process_valid_form(form)
            url = reverse("goal_detail", kwargs={'username': self.request.user.username, 'uuid': self.object.uuid})

            return HttpResponseRedirect(url)
        else:
            print "goal is not valid"
            snippet_count = request.POST.get('snippet_count')
            form = GoalForm(request.POST, snippet_count=snippet_count, user=request.user)

            snippets_extra = []
            snippet_count = 1
            field_count = 0

            fields = list(form)

            for content in fields:
                if (content.name != 'snippet_title_0' and 'snippet_title_' in content.name) or \
                        (content.name != 'snippet_language_0' and 'snippet_language_' in content.name) or \
                                        content.name != 'snippet_text_0' and 'snippet_text_' in content.name:
                    snippets_extra.append({'field': content, 'count': snippet_count})
                    field_count += 1
                    if field_count == 3:
                        snippet_count += 1
                        field_count = 0

            snippet_ranges = range(1, int(snippet_count))

        return render(request, self.template_name, {'form': form,
                                                    'snippets_extra': snippets_extra,
                                                    'snippet_ranges': snippet_ranges})


class GoalUpdate(LoginRequiredMixin, UpdateView):
    model = Goal
    template_name = "goals/goal_update_form.html"
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    form_class = GoalForm
    current_user_permissions = 3

    def get_success_url(self):
        url = reverse("goal_detail", kwargs={'uuid': self.get_object().uuid,
                                             'username': self.get_object().owner.username})
        return url

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        try:
            self.object = self.get_object()
            if request.user.pk is not self.get_object().owner.pk and \
                    (self.get_object().is_private or not self.current_user_can_view_goal()):
                raise Http404
        except Http404:
            return HttpResponseRedirect(self.get_success_url())

        form = self.get_form(self.get_form_class())

        snippet_ranges = range(1, len(self.get_object().snippets.all()))

        # print snippet_count
        snippets_extra = []
        snippet_count = 1
        field_count = 0

        fields = list(form)
        # print fields

        for content in fields:
            if (content.name != 'snippet_title_0' and 'snippet_title_' in content.name) or \
                    (content.name != 'snippet_language_0' and 'snippet_language_' in content.name) or \
                                    content.name != 'snippet_text_0' and 'snippet_text_' in content.name:
                print content.name
                snippets_extra.append({'field': content, 'count': snippet_count})
                field_count += 1
                if field_count == 3:
                    snippet_count += 1
                    field_count = 0

        return render(request, self.template_name, {'form': form,
                                                    'snippets_extra': snippets_extra,
                                                    'snippet_ranges': snippet_ranges,
                                                    'goal': self.get_object()})

    def current_user_can_view_goal(self):

        workspace_permission = WorkspacePermissions.objects.get(workspace=self.get_object().workspaces.all,
                                                                group__users=self.request.user)
        print "can view workspace?"
        print workspace_permission.permission
        if workspace_permission:
            self.current_user_permissions = workspace_permission.permission
            if self.current_user_permissions < 2:
                return True
        return False

    def get_form_kwargs(self, **kwargs):
        kwargs = super(GoalUpdate, self).get_form_kwargs(**kwargs)
        # Returns the keyword arguments for instantiating the form.

        kwargs.update({
            'saved_goal': self.get_object(),
            'snippet_count': len(self.get_object().snippets.all()),
            'user': self.request.user
        }
        )

        return kwargs

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        # print request.POST.get('snippet_count')
        form = GoalForm(request.POST, snippet_count=request.POST.get('snippet_count'), user=request.user)
        if form.is_valid():
            print "is_valid"
            self.form_valid(form)
            url = reverse("goal_detail",
                          kwargs={'username': self.request.user.username, 'uuid': self.get_object().uuid})

            return HttpResponseRedirect(url)
        else:
            print "goal is not valid"
            snippet_count = request.POST.get('snippet_count')
            form = GoalForm(request.POST, snippet_count=snippet_count, user=request.user)
            # print snippet_count
            snippets_extra = []
            snippet_count = 1
            field_count = 0

            fields = list(form)
            # print fields

            for content in fields:
                if (content.name != 'snippet_title_0' and 'snippet_title_' in content.name) or \
                        (content.name != 'snippet_language_0' and 'snippet_language_' in content.name) or \
                                        content.name != 'snippet_text_0' and 'snippet_text_' in content.name:
                    #print content.name
                    snippets_extra.append({'field': content, 'count': snippet_count})
                    field_count += 1
                    if field_count == 3:
                        snippet_count += 1
                        field_count = 0

            print snippet_count
            #snippet_count = request.POST.get('snippet_count')
            snippet_ranges = range(1, int(snippet_count))
            print snippet_ranges

        return render(request, self.template_name, {'form': form,
                                                    'snippets_extra': snippets_extra,
                                                    'snippet_ranges': snippet_ranges,
                                                    'object': self.get_object()})

    def add_new_snippets(self, form, largest_snippet_count, smallest_snippet_count):
        if smallest_snippet_count != largest_snippet_count:
            for i in range(smallest_snippet_count, largest_snippet_count):
                new_snippet = Snippet()
                new_snippet.goal = self.object
                new_snippet.author = self.request.user
                new_snippet.title = form.cleaned_data['snippet_title_' + str(i)]
                new_snippet.language = form.cleaned_data['snippet_language_' + str(i)]
                new_snippet.text = form.cleaned_data['snippet_text_' + str(i)]
                new_snippet.save()

    def add_version_of_snippet(self, snippet):
        if snippet.versions.count() > VERSIONS_LIMIT:
            snippet.versions.all()[0].delete()
        version = Version()
        version.snippet = snippet
        version.author = self.request.user
        version.language = snippet.language
        version.text = snippet.text
        version.version_number = snippet.version_number
        version.save()

    def update_existing_snippets(self, form, smallest_snippet_count):
        for i in range(smallest_snippet_count):
            snippet = self.object.snippets.all()[i]
            # Se crea la versión solo si cambian sus campos
            if snippet.text != form.cleaned_data['snippet_text_' + str(i)] or \
                            snippet.language != form.cleaned_data['snippet_language_' + str(i)] or \
                            snippet.title != form.cleaned_data['snippet_title_' + str(i)]:
                self.add_version_of_snippet(snippet)

                snippet.title = form.cleaned_data['snippet_title_' + str(i)]
                snippet.language = form.cleaned_data['snippet_language_' + str(i)]
                snippet.text = form.cleaned_data['snippet_text_' + str(i)]
                snippet.version_number += 1
                snippet.save()

    def remove_deleted_snippets(self, current_snippet_count, previous_snippet_count):
        smallest_snippet_count = previous_snippet_count
        largest_snippet_count = current_snippet_count
        if current_snippet_count < previous_snippet_count:
            for i in range(current_snippet_count, previous_snippet_count):
                snippet = self.object.snippets.all()[i]
                snippet.delete()
                smallest_snippet_count = current_snippet_count
                largest_snippet_count = previous_snippet_count - 1
        return largest_snippet_count, smallest_snippet_count

    def form_valid(self, form):
        self.object = self.get_object()

        self.object.title = form.cleaned_data['title']
        self.object.description = form.cleaned_data['description']

        if self.request.user.pk is self.object.owner.pk:
            self.object.workspaces = form.cleaned_data['workspaces']
            self.object.tags = form.cleaned_data['tags']
            self.object.is_private = form.cleaned_data['is_private']

        previous_snippet_count = len(self.object.snippets.all())
        current_snippet_count = int(form.cleaned_data['snippet_count'])

        largest_snippet_count, smallest_snippet_count = self.remove_deleted_snippets(current_snippet_count,
                                                                                     previous_snippet_count)

        self.update_existing_snippets(form, smallest_snippet_count)
        self.add_new_snippets(form, largest_snippet_count, smallest_snippet_count)

        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


class GoalList(LoginRequiredMixin, ListView):
    model = Workspace
    template_name = 'goals/goal_list.html'

    def get_queryset(self):
        return Goal.objects.filter(owner=self.request.user)

    def get(self, request, *args, **kwargs):
        current_username = self.kwargs['username']

        form = GoalSearchForm()

        if not self.request.user.is_authenticated() or self.request.user.username != current_username:
            return HttpResponseRedirect('/')

        else:
            #return super(GoalList, self).get(request, *args, **kwargs)
            return render(request, self.template_name, {'form': form, 'goal_list': self.get_queryset()})


class GoalDelete(LoginRequiredMixin, DeleteView):
    model = Goal
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    http_method_names = [u'post']

    def get_success_url(self):
        return reverse("goals_list", kwargs={'username': self.request.user.username})

    def get_object(self, queryset=None):

        uuid = self.kwargs.get(self.slug_url_kwarg, None)

        if not valid_uuid(uuid):
            raise Http404

        obj = super(GoalDelete, self).get_object()
        if not obj.owner == self.request.user:
            raise Http404
        messages.add_message(self.request, messages.SUCCESS, _("Goal deleted successfully"))
        return obj


class GoalDetail(LoginRequiredMixin, DetailView):
    model = Goal
    template_name = "goals/goal_details.html"
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    current_user_permissions = 3

    def get_success_url(self):
        url = reverse("goal_detail",
                      kwargs={'uuid': self.get_object().uuid, 'username': self.get_object().owner.username})
        return url

    def get_context_data(self, **kwargs):

        context = super(GoalDetail, self).get_context_data(**kwargs)

        if self.request.user.pk is self.get_object().owner.pk:
            self.current_user_permissions = 0

        context['current_user_permissions'] = self.current_user_permissions
        context['WORKSPACE_PERMISSIONS'] = WORKSPACE_PERMISSIONS

        return context

    def get(self, request, *args, **kwargs):

        try:
            self.object = self.get_object()
            if not self.request.user.is_authenticated() or (request.user.pk is not self.get_object().owner.pk
                                                            and (self.get_object().is_private
                                                                 or not self.current_user_can_view_goal())):
                raise Http404
        except Http404:
            return HttpResponseRedirect("/")

        context = self.get_context_data(object=self.get_object())

        if self.get_object() is not None:
            return self.render_to_response(context)
        else:
            return HttpResponseRedirect("/")

    def current_user_can_view_goal(self):

        workspace_permission = WorkspacePermissions.objects.get(workspace=self.get_object().workspaces.all,
                                                                group__users=self.request.user)
        if workspace_permission:
            self.current_user_permissions = workspace_permission.permission
            return True
        return False


class GoalDownload(LoginRequiredMixin, TemplateView):
    template_name = 'goals/download_data.html'

    def get(self, request, *args, **kwargs):
        url = reverse("profile", kwargs={'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):

        goal_uuid = kwargs.pop('uuid', None)
        goal_username = kwargs.pop('username', None)
        if goal_username is not None:
            goal_username = MyUser.objects.get(username=goal_username)

        goal = get_object_or_404(Goal, owner=goal_username, uuid=goal_uuid)

        content = ''
        for snippet in goal.snippets.all():
            content += '#/*' + snippet.title + '*\\' + '\n'
            content += snippet.text + '\n'
            content += '\n'
        res = HttpResponse(content)
        res['Content-Disposition'] = 'attachment; filename=goal.txt'
        return res