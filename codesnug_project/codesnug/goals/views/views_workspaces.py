from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError
from django.http import HttpResponseRedirect, Http404, HttpResponseNotFound
from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView
from braces.views import LoginRequiredMixin
from django.utils.translation import ugettext_lazy as _
import logging

from codesnug.goals.forms import WorkspaceUpdateForm, WorkspacePermissionsForm
from codesnug.goals.mixins import RemoveGoalFromListMixin, CheckOwnerMixin
from codesnug.goals.models import Workspace, WorkspacePermissions
from codesnug.settings import WORKSPACE_PERMISSIONS
from codesnug.users.models import UserGroup
from codesnug.utils import valid_uuid

__author__ = 'rene'


logger = logging.getLogger(__name__)


class WorkspaceCreate(LoginRequiredMixin, CreateView):
    model = Workspace
    fields = ['title', 'description']
    template_name = 'goals/workspace_form.html'

    def get_success_url(self):
        url = reverse("workspace_detail",
                      kwargs={'uuid': self.object.uuid,
                              'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        try:
            self.object.save()
        except IntegrityError:
            messages.add_message(self.request, messages.ERROR, _("Already exists a tag with that name"))
            return render(self.request, self.template_name, {'form': form})
        return self.get_success_url()


class WorkspaceDetail(LoginRequiredMixin, RemoveGoalFromListMixin, DetailView):
    model = Workspace
    template_name = "goals/workspace_details.html"
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    current_user_permissions = 3

    def get_object(self, queryset=None):

        uuid = self.kwargs.get(self.slug_url_kwarg, None)

        if not valid_uuid(uuid):
            raise Http404

        current_workspace = super(WorkspaceDetail, self).get_object()
        if not current_workspace.owner == self.request.user and not self.current_user_can_view_workspace(current_workspace):
            raise Http404
        return current_workspace

    def get_context_data(self, **kwargs):
        context = super(WorkspaceDetail, self).get_context_data(**kwargs)

        if self.request.user.pk is self.get_object().owner.pk:
            self.current_user_permissions = 0

        context['current_user_permissions'] = self.current_user_permissions
        context['WORKSPACE_PERMISSIONS'] = WORKSPACE_PERMISSIONS

        return context

    def current_user_can_view_workspace(self, current_workspace):
        workspace_permissions = WorkspacePermissions.objects.filter(workspace=current_workspace,
                                                                    group__users=self.request.user)
        logger.debug("Can view workspace")
        for workspace_permission in workspace_permissions:
            print workspace_permission.workspace.pk
            print current_workspace.pk
            if workspace_permission.workspace.pk == current_workspace.pk:
                self.current_user_permissions = workspace_permission.permission
                return True
        return False

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        context = self.get_context_data(object=self.get_object())
        print "Workspace Details"
        if self.object is not None:
            return self.render_to_response(context)
        else:
            raise Http404

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        url = reverse("workspace_detail",
                      kwargs={'uuid': self.object.uuid, 'username': self.object.owner.username})

        if 'goal_remove_id' in request.POST:
            if self.remove_goal_from_list(self.object.goals, request):
                messages.add_message(request, messages.SUCCESS, _("Goal was removed successfully from workspace"))
            else:
                messages.add_message(request, messages.ERROR, _("Goal can not be removed from workspace"))

        return HttpResponseRedirect(url)


class WorkspaceUpdate(LoginRequiredMixin, UpdateView, CheckOwnerMixin):
    model = Workspace
    template_name = "goals/workspace_update_form.html"
    fields = ['title', 'description']
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    form_class = WorkspaceUpdateForm

    def get(self, request, *args, **kwargs):
        if not self.is_owner_current_user():
            raise Http404
        else:
            return super(WorkspaceUpdate, self).get(request, *args, **kwargs)

    def get_success_url(self):
        url = reverse("workspace_detail",
                      kwargs={'uuid': self.get_object().uuid,
                              'username': self.get_object().owner.username})
        return url

    def post(self, request, *args, **kwargs):

        if not self.is_owner_current_user():
            raise Http404

        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            messages.add_message(self.request, messages.SUCCESS, _("Workspace updated successfully"))
            return self.form_valid(form)
        else:
            messages.add_message(self.request, messages.ERROR, _("Workspace can not be updated"))
            return self.form_invalid(form)


class WorkspaceDelete(LoginRequiredMixin, DeleteView):
    model = Workspace
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    http_method_names = [u'post']

    def get_success_url(self):
        return reverse("workspaces_list", kwargs={'username': self.request.user.username})

    def get_object(self, queryset=None):

        uuid = self.kwargs.get(self.slug_url_kwarg, None)

        if not valid_uuid(uuid):
            raise Http404

        obj = super(WorkspaceDelete, self).get_object()
        if not obj.owner == self.request.user:
            raise Http404
        messages.add_message(self.request, messages.SUCCESS, _("Workspace deleted successfully"))
        return obj


class WorkspaceList(LoginRequiredMixin, ListView):
    model = Workspace
    template_name = 'goals/workspace_list.html'

    def get_context_data(self, **kwargs):
        context = super(WorkspaceList, self).get_context_data(**kwargs)
        usergroups_where_is_current_user = UserGroup.objects.filter(users=self.request.user)
        workspace_permissions = WorkspacePermissions.objects.filter(group=usergroups_where_is_current_user)

        context['others_workspaces'] = workspace_permissions
        context['workspace_permissions'] = WORKSPACE_PERMISSIONS

        return context

    def get_queryset(self):
        return Workspace.objects.filter(owner=self.request.user)

    def get(self, request, *args, **kwargs):
        current_username = kwargs.pop('username', None)
        if not self.request.user.is_authenticated() or self.request.user.username != current_username:
            return HttpResponseRedirect('/')

        else:
            return super(WorkspaceList, self).get(request, *args, **kwargs)


class WorkspacePermissionsCreate(LoginRequiredMixin, CreateView):
    model = WorkspacePermissions
    fields = ['permission', 'workspace']
    template_name = 'users/add_workspace_to_group_form.html'

    form_class = WorkspacePermissionsForm

    def user_group_add_is_valid(self, user_group, **kwargs):

        group = UserGroup.objects.get(uuid=user_group, owner=self.request.user)

        if group is None:
            return None

        return group

    def get(self, request, *args, **kwargs):
        if 'uuid' in kwargs:
            user_group_add = kwargs.get('uuid', None)

            if user_group_add is not None:
                group_to_add = self.user_group_add_is_valid(user_group_add)
                if group_to_add is not None:
                    form = WorkspacePermissionsForm(user=self.request.user, initial={
                        'usergroup': group_to_add.uuid
                    })
                    return render(request, self.template_name, {'object': group_to_add, 'form': form})
        url = reverse('goals_list', kwargs={'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):

        usergroup_uuid = request.POST.get('usergroup', None)

        if not valid_uuid(usergroup_uuid):
            messages.add_message(request, messages.ERROR, "Invalid data")

            url = reverse("usergroup_detail",
                          kwargs={'uuid': usergroup_uuid, 'username': self.request.user.username})

            return HttpResponseRedirect(url)

        form = self.form_class(self.request.user, request.POST)
        if form.is_valid():

            logger.debug("form is valid!")

            url = reverse("usergroup_detail",
                          kwargs={'uuid': form.cleaned_data['usergroup'], 'username': self.request.user.username})
            try:
                self.save(form)
                messages.add_message(request, messages.SUCCESS, _("Workspace added to group successfully"))

            except IntegrityError:

                group_to_add = self.user_group_add_is_valid(form.cleaned_data['usergroup'])
                messages.add_message(request, messages.ERROR, _("A workspace can be added only once to a group"))
                return render(request, self.template_name, {
                    'form': form,
                    'object': group_to_add})

            return HttpResponseRedirect(url)

        print "is not valid"

        group_to_add = self.user_group_add_is_valid(form.cleaned_data['usergroup'])

        if group_to_add:
            return render(request, self.template_name, {'form': form, 'object': group_to_add})
        url = reverse('goals_list', kwargs={'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def save(self, form):
        print "save"

        group_to_add = self.user_group_add_is_valid(form.cleaned_data['usergroup'])

        if group_to_add is None:
            url = reverse('goals_list', kwargs={'username': self.request.user.username})
            return HttpResponseRedirect(url)

        print 'Salvando...'
        workspace_permissions = WorkspacePermissions()
        workspace_permissions.permission = form.cleaned_data['permission']
        workspace_permissions.workspace = form.cleaned_data['workspace']
        workspace_permissions.group = group_to_add
        workspace_permissions.save()
        return

    def get_success_url(self):
        return self.success_url