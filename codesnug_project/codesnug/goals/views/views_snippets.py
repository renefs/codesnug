__author__ = 'rene'
from django.views.generic import DetailView
from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.contrib import messages
import logging
from django.http import Http404
from braces.views import LoginRequiredMixin

from codesnug.settings import WORKSPACE_PERMISSIONS, VERSIONS_LIMIT
from codesnug.goals.models import WorkspacePermissions, Snippet, Version

logger = logging.getLogger(__name__)


class SnippetDetail(LoginRequiredMixin, DetailView):
    model = Snippet
    template_name = 'goals/snippet_details.html'
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    current_user_permissions = 3

    def get_success_url(self):
        url = reverse("snippet_detail",
                      kwargs={'goal_uuid': self.get_object().goal.uuid, 'uuid': self.get_object().uuid,
                              'username': self.get_object().goal.owner.username})
        return url

    def get_context_data(self, **kwargs):

        context = super(SnippetDetail, self).get_context_data(**kwargs)

        if self.request.user.pk is self.get_object().goal.owner.pk:
            self.current_user_permissions = 0

        context['current_user_permissions'] = self.current_user_permissions
        context['WORKSPACE_PERMISSIONS'] = WORKSPACE_PERMISSIONS
        context['goal'] = self.get_object().goal

        return context

    def current_user_can_view_goal(self):

        workspace_permission = WorkspacePermissions.objects.get(workspace=self.get_object().goal.workspaces.all,
                                                                group__users=self.request.user)
        print workspace_permission.permission
        if workspace_permission:
            self.current_user_permissions = workspace_permission.permission
            if self.current_user_permissions < WORKSPACE_PERMISSIONS[2][0]:
                return True
        return False

    def get(self, request, *args, **kwargs):

        try:
            self.object = self.get_object()
            if not self.request.user.is_authenticated() or (
                            request.user.pk is not self.get_object().goal.owner.pk and not self.current_user_can_view_goal()):
                raise Http404
        except Http404:
            url = reverse("goal_detail", kwargs={'uuid': self.get_object().goal.uuid,
                                                 'username': self.get_object().goal.owner.username})
            print "URL: " + url
            return HttpResponseRedirect(url)

        return super(SnippetDetail, self).get(request, *args, **kwargs)

    def restore_snippet_version(self, request):
        self.object = self.get_object()
        version_restore_id = request.POST.get('version_restore_id', None)
        version = self.get_object().versions.get(uuid=version_restore_id)
        # Nueva version
        new_version = Version()
        new_version.snippet = self.object
        new_version.author = self.request.user
        new_version.language = self.object.language
        new_version.text = self.object.text
        new_version.version_number = self.object.version_number

        if self.get_object().versions.count() > VERSIONS_LIMIT:
            self.get_object().versions.all()[0].delete()
        new_version.save()
        self.object.language = version.language
        self.object.text = version.text
        self.object.version_number += 1
        self.object.save()
        messages.add_message(request, messages.SUCCESS, "Snippet was restored successfully")

    def post(self, request, *args, **kwargs):

        if 'version_restore_id' in request.POST:
            self.restore_snippet_version(request)

        if 'snippet_remove_id' in request.POST:
            snippet_remove_id = request.POST.get('snippet_remove_id', None)
            snippet = Snippet.objects.get(uuid=snippet_remove_id, goal__owner=request.user)

            if self.get_object().goal.snippets.count() < 2:
                messages.add_message(request, messages.ERROR, "A goal must have at least one snippet")
                return HttpResponseRedirect(self.get_success_url())

            if snippet == self.get_object():
                snippet.delete()

                messages.add_message(request, messages.SUCCESS, "Snippet was removed successfully")

        return HttpResponseRedirect(self.get_success_url())