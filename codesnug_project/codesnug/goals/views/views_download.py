__author__ = 'rene'

from django.views.generic import TemplateView
from django.shortcuts import HttpResponseRedirect, HttpResponse, get_object_or_404
from django.core.urlresolvers import reverse


import logging
import json
from braces.views import LoginRequiredMixin
from codesnug.goals.models import Goal, Snippet
from codesnug.users.models import MyUser

logger = logging.getLogger(__name__)


class DataDownload(LoginRequiredMixin, TemplateView):
    template_name = 'goals/download_data.html'

    def get(self, request, *args, **kwargs):
        url = reverse("profile", kwargs={'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        all_goals = list(Goal.objects.filter(owner=self.request.user))

        json_data = json.dumps([{'title': o.title,
                                 'description': o.description,
                                 'creation_date': str(o.created_at),
                                 'is_private': o.is_private,
                                 'id': str(o.uuid),
                                 'snippets': [{'title': a.title,
                                               'language': a.language,
                                               'creation_date': str(a.created_at),
                                               'text': a.text,
                                               'version': a.version_number,
                                               'id': str(a.uuid),
                                               'goal_id': str(a.goal.uuid)
                                              } for a in o.snippets.all()]
                                } for o in all_goals])

        res = HttpResponse(json_data)
        res['Content-Disposition'] = 'attachment; filename=' + request.user.username + ".json"
        return res


class SnippetDownload(LoginRequiredMixin, TemplateView):
    template_name = 'goals/download_data.html'

    def get(self, request, *args, **kwargs):
        url = reverse("profile", kwargs={'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def post(self, request, *args, **kwargs):
        snippet_uuid = kwargs.pop('uuid', None)
        goal_username = kwargs.pop('username', None)

        if goal_username is not None:
            goal_username = MyUser.objects.get(username=goal_username)

        snippet = get_object_or_404(Snippet, goal__owner=goal_username, uuid=snippet_uuid)

        res = HttpResponse(snippet.text)
        res['Content-Disposition'] = 'attachment; filename=snippet.txt'
        return res