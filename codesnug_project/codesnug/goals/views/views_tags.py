from codesnug.utils import valid_uuid

__author__ = 'rene'

from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.shortcuts import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.db import IntegrityError
from django.shortcuts import render
from django.contrib import messages
import logging
from django.http import Http404
from braces.views import LoginRequiredMixin
from django.utils.translation import ugettext_lazy as _
from codesnug.goals.mixins import RemoveGoalFromListMixin, CheckOwnerMixin

from codesnug.goals.models import Tag

logger = logging.getLogger(__name__)


class TagList(LoginRequiredMixin, ListView):
    model = Tag
    template_name = 'goals/tag_list.html'

    def get_queryset(self):
        return Tag.objects.filter(owner=self.request.user)

    def get(self, request, *args, **kwargs):
        current_username = kwargs.pop('username', None)
        if not self.request.user.is_authenticated() or self.request.user.username != current_username:
            return HttpResponseRedirect(reverse('home'))
        else:
            return super(TagList, self).get(request, *args, **kwargs)


class TagCreate(LoginRequiredMixin, CreateView):
    model = Tag
    fields = ['title']
    template_name = 'goals/tag_form.html'

    def get_success_url(self):
        url = reverse("tags_list",
                      kwargs={'username': self.request.user.username})
        return HttpResponseRedirect(url)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        try:
            self.object.save()
        except IntegrityError:
            messages.add_message(self.request, messages.ERROR,
                                 "Already exists a tag with that name")
            return render(self.request, self.template_name, {'form': form})
        return self.get_success_url()


class TagDetail(LoginRequiredMixin, RemoveGoalFromListMixin, DetailView):
    model = Tag
    template_name = "goals/tag_details.html"
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    current_user_permissions = 3

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        try:
            if request.user.pk is not self.get_object().owner.pk:
                raise Http404
        except Http404:
            return HttpResponseRedirect(reverse('home'))

        context = self.get_context_data(object=self.get_object())

        if self.get_object() is not None:
            return self.render_to_response(context)
        else:
            raise Http404

    def post(self, request, *args, **kwargs):

        if self.remove_goal_from_list(self.get_object().goals, request):
            messages.add_message(request, messages.SUCCESS, _("Goal was removed successfully from tag"))
        else:
            messages.add_message(request, messages.ERROR, _("Goal can not be removed from tag"))

        url = reverse("tag_detail",
                      kwargs={'uuid': self.get_object().uuid, 'username': self.get_object().owner.username})

        return HttpResponseRedirect(url)


class TagUpdate(LoginRequiredMixin, UpdateView, CheckOwnerMixin):
    model = Tag
    template_name = "goals/tag_update_form.html"
    fields = ['title']
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'

    def get(self, request, *args, **kwargs):
        if not self.is_owner_current_user():
            raise Http404
        else:
            return super(TagUpdate, self).get(request, *args, **kwargs)

    def get_success_url(self):
        url = reverse("tag_detail", kwargs={'uuid': self.get_object().uuid, 'username': self.request.user.username})
        return url

    def post(self, request, *args, **kwargs):

        if not self.is_owner_current_user():
            raise Http404

        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            messages.add_message(self.request, messages.SUCCESS, _("Tag updated successfully"))
            return self.form_valid(form)
        else:
            messages.add_message(self.request, messages.ERROR, _("Tag can not be updated"))
            return self.form_invalid(form)


class TagDelete(LoginRequiredMixin, DeleteView):
    model = Tag
    slug_url_kwarg = 'uuid'
    slug_field = 'uuid'
    http_method_names = [u'post']

    def get_success_url(self):
        return reverse("tags_list", kwargs={'username': self.request.user.username})

    def get_object(self, queryset=None):

        uuid = self.kwargs.get(self.slug_url_kwarg, None)

        if not valid_uuid(uuid):
            raise Http404

        obj = super(TagDelete, self).get_object()
        if not obj.owner == self.request.user:
            raise Http404
        messages.add_message(self.request, messages.SUCCESS, _("Tag deleted successfully"))
        return obj