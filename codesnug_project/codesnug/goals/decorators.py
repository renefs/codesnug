__author__ = 'rene'
from django_ajax.decorators import ajax
from django.contrib.auth.decorators import login_required
from codesnug.goals.models import Workspace, Tag
from django.db import IntegrityError

@ajax
@login_required
def add_workspace_ajax(request):
    if request.user.is_authenticated():
        if 'workspace_name' in request.POST:
            workspace = Workspace()
            workspace.title = request.POST.get('workspace_name', None)
            workspace.owner = request.user

            if workspace.title == "":
                return {'id': 'null'}
            try:
                workspace.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': workspace.title, 'id': workspace.pk}

@ajax
@login_required
def add_tag_ajax(request):
    print request.POST
    if request.user.is_authenticated():
        print "Adding tag... "
        if 'tag_name' in request.POST:
            print request.POST.get('tag_name', None)
            tag = Tag()
            tag.title = request.POST.get('tag_name', None)
            tag.owner = request.user

            print tag

            if tag.title == "":
                return {'id': 'null'}
            try:
                tag.save()
            except IntegrityError as e:
                return {'id': 'null'}

            return {'title': tag.title, 'id': tag.pk}

