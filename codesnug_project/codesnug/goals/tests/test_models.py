__author__ = 'rene'

from django.test import TestCase
from django.db import IntegrityError
from codesnug.users.models import MyUser
from codesnug.goals.models import Tag, Workspace


# Create your tests here.
class TagModelTestCase(TestCase):

    user = None
    user2 = None

    def setUp(self):
        self.user = MyUser.objects.create_user('user1', 'prueba@prueba.com', '1111')
        self.user2 = MyUser.objects.create_user('user2', 'prueba2@prueba2.com', '1111')

    def tearDown(self):
        self.user=None
        self.user2=None

    def test_tags_add(self):

        tag1 = Tag.objects.create(title="tag1", owner=self.user)
        tag2 = Tag.objects.create(title="tag2", owner=self.user)

        tag3 = Tag.objects.create(title="tag1", owner=self.user2)
        tag4 = Tag.objects.create(title="tag2", owner=self.user2)

        self.assertIn(tag1, self.user.owned_tags.all())
        self.assertIn(tag2, self.user.owned_tags.all())

        self.assertTrue(self.user.owned_tags.all().count() == 2)

        self.assertIn(tag3, self.user2.owned_tags.all())
        self.assertIn(tag4, self.user2.owned_tags.all())

        self.assertTrue(self.user.owned_tags.all().count() == 2)
        self.assertTrue(self.user2.owned_tags.all().count() == 2)

        self.assertTrue(Tag.objects.all().count(), 4)

    def test_user_has_no_tags(self):
        self.assertTrue(self.user.owned_tags.all().count() == 0)

    def test_tags_with_same_same(self):

        tag1 = Tag.objects.create(title="tag1", owner=self.user)

        self.assertIn(tag1, self.user.owned_tags.all())

        error_occured = False
        try:
            Tag.objects.create(title="tag1", owner=self.user)
        except IntegrityError:
            error_occured = True

        self.assertTrue(error_occured)

    def test_tags_remove(self):
        tag1 = Tag.objects.create(title="tag1", owner=self.user)
        self.assertIn(tag1, self.user.owned_tags.all())
        self.assertTrue(self.user.owned_tags.all().count() == 1)

        tag1.delete()
        self.assertNotIn(tag1, self.user.owned_tags.all())
        self.assertTrue(self.user.owned_tags.all().count() == 0)
        self.assertTrue(Tag.objects.all().count() == 0)


class WorkspaceModelTestCase(TestCase):

    user = None
    user2 = None

    def setUp(self):
        self.user = MyUser.objects.create_user('user1', 'prueba@prueba.com', '1111')
        self.user2 = MyUser.objects.create_user('user2', 'prueba2@prueba2.com', '1111')

    def test_workspace_add(self):

        workspace1 = Workspace.objects.create(title="workspace1", owner=self.user)
        workspace2 = Workspace.objects.create(title="workspace2", owner=self.user)

        workspace3 = Workspace.objects.create(title="workspace3", owner=self.user2)
        workspace4 = Workspace.objects.create(title="workspace4", owner=self.user2)

        self.assertIn(workspace1, self.user.owned_workspaces.all())
        self.assertIn(workspace2, self.user.owned_workspaces.all())

        self.assertTrue(self.user.owned_workspaces.all().count() == 2)

        self.assertIn(workspace3, self.user2.owned_workspaces.all())
        self.assertIn(workspace4, self.user2.owned_workspaces.all())

        self.assertTrue(self.user.owned_workspaces.all().count() == 2)
        self.assertTrue(self.user2.owned_workspaces.all().count() == 2)

        self.assertTrue(Workspace.objects.all().count(), 4)

    def test_user_has_no_workspaces(self):
        self.assertTrue(self.user.owned_workspaces.all().count() == 0)

    def test_workspaces_with_same_same(self):

        tag1 = Workspace.objects.create(title="workspace1", owner=self.user)

        self.assertIn(tag1, self.user.owned_workspaces.all())

        error_occured = False
        try:
            Workspace.objects.create(title="workspace1", owner=self.user)
        except IntegrityError:
            error_occured = True

        self.assertTrue(error_occured)

    def test_workspaces_remove(self):
        workspace1 = Workspace.objects.create(title="workspace1", owner=self.user)
        self.assertIn(workspace1, self.user.owned_workspaces.all())
        self.assertTrue(self.user.owned_workspaces.all().count() == 1)

        workspace1.delete()
        self.assertNotIn(workspace1, self.user.owned_workspaces.all())
        self.assertTrue(self.user.owned_workspaces.all().count() == 0)
        self.assertTrue(Workspace.objects.all().count() == 0)





