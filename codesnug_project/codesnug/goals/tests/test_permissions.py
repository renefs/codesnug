__author__ = 'rene'

from django.core.urlresolvers import reverse
from django.test import TestCase, Client
from codesnug.users.models import MyUser, UserGroup
from codesnug.goals.models import Workspace, Goal


def build_test_url(url):
    return 'https://testserver:80' + url


class GoalPermissionsTest(TestCase):

    client = None
    user_admin = None  # admin
    user_admin_username = 'admin1'
    date_of_birth = '11/02/1984'
    user_admin_email = 'test@test.com'

    user_subscriber = None  # subscriber
    user_subscriber_username = 'subscriber'
    user_subscriber_email = 'test1@test.com'

    goal_title = 'Goal BLABLABLA'
    goal_description = 'This is the goal description'
    snippet_title_0 = 'test.py'
    snippet_language_0 = 'python'
    snippet_text_0 = 'print "Hello World, this is a test"'

    def setUp(self):
        # Every test needs a client.
        self.client = Client()
        self.create_users()

    def tearDown(self):
        self.client.get(reverse('auth_logout'))

    def create_users(self):
        self.create_admin()
        self.create_subscriber()

    def create_admin(self):
        # 1
        self.client.post(reverse('home'),
                         {'username': self.user_admin_username,
                          'password1': self.user_admin_username,
                          'password2': self.user_admin_username,
                          'date_of_birth': self.date_of_birth,
                          'email': self.user_admin_email,
                          'tos': 'yes'})
        self.user_admin = MyUser.objects.get(username=self.user_admin_username)
        self.user_admin.is_active = True
        self.user_admin.save()

    def create_subscriber(self):
        # 2
        self.client.post(reverse('home'),
                         {'username': self.user_subscriber_username,
                          'password1': self.user_subscriber_username,
                          'password2': self.user_subscriber_username,
                          'date_of_birth': self.date_of_birth,
                          'email': self.user_subscriber_email,
                          'tos': 'yes'})
        self.user_subscriber = MyUser.objects.get(username=self.user_subscriber_username)
        self.user_subscriber.is_active = True
        self.user_subscriber.save()

    def test_add_goal_admin_error(self):

        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                          })


        # adding workspace
        response = self.client.post(reverse('goals_add'),
                         {'title': self.goal_title,
                          'description': self.goal_description,
                          'snippet_title_0': self.snippet_title_0,
                          'snippet_language_0': self.snippet_language_0,
                          'snippet_text_0': self.snippet_text_0,
                          'snippet_count': 'aaa',
                          }, follow=True)

        self.assertEqual(response.status_code, 404)

        try:
            Goal.objects.get(title=self.goal_title)
            self.assertTrue(True, "Goal should not be found")
        except Goal.DoesNotExist:
            pass

        self.assertEqual(Goal.objects.all().count(), 0)

    def test_add_goal_admin(self):

        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                          })

        response = self.client.post(reverse('goals_add'),
                                    {'title': self.goal_title,
                                     'description': self.goal_description,
                                     'snippet_title_0': self.snippet_title_0,
                                     'snippet_language_0': self.snippet_language_0,
                                     'snippet_text_0': self.snippet_text_0,
                                     'snippet_count': 1,
                                     }, follow=True)

        new_goal = Goal.objects.get(title=self.goal_title)

        self.assertRedirects(response, build_test_url(reverse('goal_detail',
                                                              kwargs={'username': self.user_admin_username,
                                                                      'uuid': new_goal.uuid})))

        self.assertEqual(new_goal.description, self.goal_description)
        self.assertEqual(Goal.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)

    def test_add_goal_guest_error(self):

        # adding workspace
        response = self.client.post(reverse('goals_add'),
                                    {'title': self.goal_title,
                                     'description': self.goal_description,
                                     'snippet_title_0': self.snippet_title_0,
                                     'snippet_language_0': self.snippet_language_0,
                                     'snippet_text_0': self.snippet_text_0,
                                     'snippet_count': 1,
                                     }, follow=True)

        try:
            Goal.objects.get(title=self.goal_title)
            self.assertTrue(True, "Goal should not be found")
        except Goal.DoesNotExist:
            pass

        self.assertEqual(Goal.objects.all().count(), 0)
        self.assertEqual(response.status_code, 200)

    def test_remove_goal_owner(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                          })

        response = self.client.post(reverse('goals_add'),
                                    {'title': self.goal_title,
                                     'description': self.goal_description,
                                     'snippet_title_0': self.snippet_title_0,
                                     'snippet_language_0': self.snippet_language_0,
                                     'snippet_text_0': self.snippet_text_0,
                                     'snippet_count': 1,
                                     }, follow=True)

        new_goal = Goal.objects.get(title=self.goal_title)

        self.assertRedirects(response, build_test_url(reverse('goal_detail',
                                                              kwargs={'username': self.user_admin_username,
                                                                      'uuid': new_goal.uuid})))

        self.assertEqual(new_goal.description, self.goal_description)
        self.assertEqual(Goal.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(reverse('goal_delete',
                                    kwargs={'username': self.user_admin_username,
                                            'uuid': new_goal.uuid}), follow=True)

        self.assertEqual(Goal.objects.all().count(), 0)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'alert-success', 1, 200)
        self.assertContains(response, 'alert-error', 0, 200)

    def test_remove_goal_not_owner(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                          })

        response = self.client.post(reverse('goals_add'),
                                    {'title': self.goal_title,
                                     'description': self.goal_description,
                                     'snippet_title_0': self.snippet_title_0,
                                     'snippet_language_0': self.snippet_language_0,
                                     'snippet_text_0': self.snippet_text_0,
                                     'snippet_count': 1,
                                     }, follow=True)

        new_goal = Goal.objects.get(title=self.goal_title)

        self.assertRedirects(response, build_test_url(reverse('goal_detail',
                                                              kwargs={'username': self.user_admin_username,
                                                                      'uuid': new_goal.uuid})))

        self.assertEqual(new_goal.description, self.goal_description)
        self.assertEqual(Goal.objects.all().count(), 1)
        self.assertEqual(response.status_code, 200)

        self.client.get(reverse('auth_logout'))

        self.client.post(reverse('auth_login'),
                         {'username': self.user_subscriber_username,
                          'password': self.user_subscriber_username,
                          })

        #Using admin username
        response = self.client.post(reverse('goal_delete',
                                            kwargs={'username': self.user_admin_username,
                                                    'uuid': new_goal.uuid}), follow=True)

        self.assertEqual(Goal.objects.all().count(), 1)
        self.assertEqual(response.status_code, 404)

        self.assertContains(response, 'alert-success', 0, 404)
        self.assertContains(response, 'alert-error', 0, 404)


        #Using subscriber username
        response = self.client.post(reverse('goal_delete',
                                            kwargs={'username': self.user_subscriber_username,
                                                    'uuid': new_goal.uuid}), follow=True)

        self.assertEqual(Goal.objects.all().count(), 1)
        self.assertEqual(response.status_code, 404)


class WorkspacePermissionsTest(TestCase):
    client = None
    user_admin = None  # admin
    user_admin_username = 'admin1'
    date_of_birth = '11/02/1984'
    user_admin_email = 'test@test.com'

    user_subscriber = None  # subscriber
    user_subscriber_username = 'subscriber'
    user_subscriber_email = 'test1@test.com'

    user_editor = None  # editor
    user_editor_username = 'editor'
    user_editor_email = 'test2@test.com'

    user_none = None  # none
    user_none_username = 'none'
    user_none_email = 'test3@test.com'

    workspace = None
    workspace_title = 'Workspace User Admin'

    subscribers_group = None
    subscribers_group_title = 'Subscribers Usergroup User Admin'

    editors_group = None
    editors_group_title = 'Usergroup User Admin'

    subscriber_permission = 2
    editor_permission = 1

    goal = None
    goal_title = 'Goal BLABLABLA'
    goal_description = 'This is the goal description'
    snippet_title_0 = 'test.py'
    snippet_language_0 = 'python'
    snippet_text_0 = 'print "Hello World, this is a test"'

    # Hack. Reverse not working ok for redirects? WHY? WHYYYYYYYY?
    def build_test_url(self, url):
        return 'https://testserver:80' + url

    def create_admin(self):
        # 1
        self.client.post(reverse('home'),
                         {'username': self.user_admin_username,
                          'password1': self.user_admin_username,
                          'password2': self.user_admin_username,
                          'date_of_birth': self.date_of_birth,
                          'email': self.user_admin_email,
                          'tos': 'yes'})
        self.user_admin = MyUser.objects.get(username=self.user_admin_username)
        self.user_admin.is_active = True
        self.user_admin.save()

    def create_subscriber(self):
        # 2
        self.client.post(reverse('home'),
                         {'username': self.user_subscriber_username,
                          'password1': self.user_subscriber_username,
                          'password2': self.user_subscriber_username,
                          'date_of_birth': self.date_of_birth,
                          'email': self.user_subscriber_email,
                          'tos': 'yes'})
        self.user_subscriber = MyUser.objects.get(username=self.user_subscriber_username)
        self.user_subscriber.is_active = True
        self.user_subscriber.save()

    def create_editor(self):
        # 3
        self.client.post(reverse('home'),
                         {'username': self.user_editor_username,
                          'password1': self.user_editor_username,
                          'password2': self.user_editor_username,
                          'date_of_birth': self.date_of_birth,
                          'email': self.user_editor_email,
                          'tos': 'yes'})
        self.user_editor = MyUser.objects.get(username=self.user_editor_username)
        self.user_editor.is_active = True
        self.user_editor.save()

    def create_user_none(self):
        # 3
        self.client.post(reverse('home'),
                         {'username': self.user_none_username,
                          'password1': self.user_none_username,
                          'password2': self.user_none_username,
                          'date_of_birth': self.date_of_birth,
                          'email': self.user_none_email,
                          'tos': 'yes'})
        self.user_none = MyUser.objects.get(username=self.user_none_username)
        self.user_none.is_active = True
        self.user_none.save()

    def create_users(self):
        self.create_admin()
        self.create_subscriber()
        self.create_editor()
        self.create_user_none()

    def add_workspace(self):
        # adding workspace
        self.client.post(reverse('workspaces_add'),
                         {'title': self.workspace_title,
                          'description': 'Workspace description for User',
                         }, follow=True)
        self.workspace = Workspace.objects.get(title=self.workspace_title)

    def add_usergroup_subscribers(self):
        # adding usergroup for subscribers
        self.client.post(reverse('usergroups_add'), {'title': self.subscribers_group_title})
        self.subscribers_group = UserGroup.objects.get(title=self.subscribers_group_title)
        # add user 2 to subscribers group
        self.client.post(reverse('asign_user_group',
                                 kwargs={'username': self.user_admin.username,
                                         'uuid': self.subscribers_group.uuid},
        ), {'usergroup': self.subscribers_group.uuid,
            'user': self.user_subscriber.username})

        #add workspace to subscribers group with subscribers permission
        self.client.post(reverse('asign_workspace_group',
                                 kwargs={'username': self.user_admin.username,
                                         'uuid': self.subscribers_group.uuid},
        ), {'usergroup': self.subscribers_group.uuid,
            'workspace': self.workspace.pk,
            'permission': self.subscriber_permission})

    def add_usergroup_editors(self):
        # adding usergroup for editors
        self.client.post(reverse('usergroups_add'), {'title': self.editors_group_title})
        self.editors_group = UserGroup.objects.get(title=self.editors_group_title)
        # add user 3 to editors group
        self.client.post(reverse('asign_user_group',
                                 kwargs={'username': self.user_admin.username,
                                         'uuid': self.editors_group.uuid},
        ), {'usergroup': self.editors_group.uuid,
            'user': self.user_editor.username})
        #add workspace to editors group with editors permission
        self.client.post(reverse('asign_workspace_group',
                                 kwargs={'username': self.user_admin.username,
                                         'uuid': self.editors_group.uuid},
        ), {'usergroup': self.editors_group.uuid,
            'workspace': self.workspace.pk,
            'permission': self.editor_permission})

    def create_workspaces_and_usergroups(self):
        # Login with admin
        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin.username,
                          'password': self.user_admin.username}, follow=True)

        self.add_workspace()
        self.add_goal_to_workspace()
        self.add_usergroup_subscribers()
        self.add_usergroup_editors()

        self.client.get(reverse('auth_logout'))

    def add_goal_to_workspace(self):

        workspaces = []
        workspaces.append(self.workspace.pk)

        response = self.client.post(reverse('goals_add'),
                                    {'title': self.goal_title,
                                     'description': self.goal_description,
                                     'workspaces': self.workspace.pk,
                                     'snippet_title_0': self.snippet_title_0,
                                     'snippet_language_0': self.snippet_language_0,
                                     'snippet_text_0': self.snippet_text_0,
                                     'snippet_count': 1,
                                     'is_private': False
                                     }, follow=True)

        self.goal = Goal.objects.get(title=self.goal_title)
        self.workspace = Workspace.objects.get(title=self.workspace_title)

        self.assertEqual(self.workspace.goals.all().count(), 1)
        self.assertEqual(self.goal.workspaces.all().count(), 1)

        self.assertRedirects(response, build_test_url(reverse('goal_detail',
                                                              kwargs={'username': self.user_admin_username,
                                                                      'uuid': self.goal.uuid})))

        self.assertEqual(self.goal.description, self.goal_description)
        self.assertEqual(Goal.objects.all().count(), 1)

    def setUp(self):
        # Every test needs a client.
        self.client = Client()
        self.create_users()
        self.create_workspaces_and_usergroups()

    def tearDown(self):
        self.client.get(reverse('auth_logout'))

    def test_workspace_access_admin(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                         })

        response = self.client.get(reverse('workspace_detail',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 200)

    def test_workspace_access_subscriber(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_subscriber_username,
                          'password': self.user_subscriber_username,
                         })

        response = self.client.get(reverse('workspace_detail',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 200)

    def test_workspace_access_editor(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_editor_username,
                          'password': self.user_editor_username,
                         })

        response = self.client.get(reverse('workspace_detail',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 200)

    def test_workspace_access_none(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_none_username,
                          'password': self.user_none_username,
                         })

        response = self.client.get(reverse('workspace_detail',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 404)
        self.client.get(reverse('auth_logout'))

    def test_workspace_access_guest(self):
        response = self.client.get(reverse('workspace_detail',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))

        # login with next argument
        self.assertEqual(response.status_code, 302)

    def test_workspace_update_admin(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                         })

        #get
        response = self.client.get(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 200)

        #post
        response = self.client.post(reverse('workspace_update',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}),
                                   {'title': 'New Title', 'description': 'New Description'}, follow=True)

        self.assertRedirects(response, build_test_url(reverse('workspace_detail',
                                                              kwargs={'username': self.user_admin.username,
                                                                      'uuid': self.workspace.uuid})))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertContains(response, 'alert-error', 0, 200)

    def test_workspace_update_subscriber(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_subscriber_username,
                          'password': self.user_subscriber_username,
                          })

        #get
        response = self.client.get(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 404)

        #post
        response = self.client.post(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}),
                                    {'title': 'New Title', 'description': 'New Description'})

        self.assertEqual(response.status_code, 404)

    def test_workspace_update_editor(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_editor_username,
                          'password': self.user_editor_username,
                          })

        #get
        response = self.client.get(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 404)

        #post
        response = self.client.post(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}),
                                    {'title': 'New Title', 'description': 'New Description'})

        self.assertEqual(response.status_code, 404)

    def test_workspace_update_none(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_none_username,
                          'password': self.user_none_username,
                          })

        #get
        response = self.client.get(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))
        self.assertEqual(response.status_code, 404)

        #post
        response = self.client.post(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}),
                                    {'title': 'New Title', 'description': 'New Description'})

        self.assertEqual(response.status_code, 404)

    def test_workspace_update_guest(self):

        #get
        response = self.client.get(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))
        # login with next argument
        self.assertEqual(response.status_code, 302)
        self.assertContains(response, 'alert-success', 0, 302)
        self.assertContains(response, 'alert-error', 0, 302)

        #post
        response = self.client.post(reverse('workspace_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}),
                                    {'title': 'New Title', 'description': 'New Description'})

        # login with next argument
        self.assertEqual(response.status_code, 302)
        self.assertContains(response, 'alert-success', 0, 302)
        self.assertContains(response, 'alert-error', 0, 302)

    def test_workspace_delete_admin_get(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                          })

        #get
        response = self.client.get(reverse('workspace_delete',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))

        self.assertEqual(response.status_code, 405)

    def test_workspace_delete_admin_post(self):

        response = self.client.post(reverse('auth_login'),
                         {'username': self.user_admin_username,
                          'password': self.user_admin_username,
                          }, follow=True)

        self.assertRedirects(response,
                             build_test_url(reverse('goals_list', kwargs={'username': self.user_admin.username})))

        self.assertEqual(Workspace.objects.all().count(), 1)

        #post
        response = self.client.post(reverse('workspace_delete',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}), follow=True)

        self.assertRedirects(response, build_test_url(reverse('workspaces_list', kwargs={'username': self.user_admin.username})))

        self.assertEqual(Workspace.objects.all().count(), 0)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-success', 1, 200)
        self.assertContains(response, 'alert-error', 0, 200)

    def test_workspace_delete_subscriber_get(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_subscriber_username,
                          'password': self.user_subscriber_username,
                          })

        #get
        response = self.client.get(reverse('workspace_delete',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))

        self.assertEqual(response.status_code, 405)

    def test_workspace_delete_subscriber_post(self):

        self.client.post(reverse('auth_login'),
                                    {'username': self.user_subscriber_username,
                                     'password': self.user_subscriber_username,
                                     }, follow=True)

        self.assertEqual(Workspace.objects.all().count(), 1)

        #post
        response = self.client.post(reverse('workspace_delete',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}), follow=True)

        self.assertEqual(Workspace.objects.all().count(), 1)

        self.assertEqual(response.status_code, 404)
        self.assertContains(response, 'alert-success', 0, 404)
        self.assertContains(response, 'alert-error', 0, 404)

    def test_workspace_delete_editor_get(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_editor_username,
                          'password': self.user_editor_username,
                          })

        #get
        response = self.client.get(reverse('workspace_delete',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))

        self.assertEqual(response.status_code, 405)

    def test_workspace_delete_editor_post(self):

        self.client.post(reverse('auth_login'),
                         {'username': self.user_editor_username,
                          'password': self.user_editor_username,
                          }, follow=True)

        self.assertEqual(Workspace.objects.all().count(), 1)

        #post
        response = self.client.post(reverse('workspace_delete',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}), follow=True)

        self.assertEqual(Workspace.objects.all().count(), 1)

        self.assertEqual(response.status_code, 404)
        self.assertContains(response, 'alert-success', 0, 404)
        self.assertContains(response, 'alert-error', 0, 404)

    def test_workspace_delete_none_get(self):
        self.client.post(reverse('auth_login'),
                         {'username': self.user_none_username,
                          'password': self.user_none_username,
                          })

        #get
        response = self.client.get(reverse('workspace_delete',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))

        self.assertEqual(response.status_code, 405)

    def test_workspace_delete_none_post(self):

        self.client.post(reverse('auth_login'),
                         {'username': self.user_none_username,
                          'password': self.user_none_username,
                          }, follow=True)

        self.assertEqual(Workspace.objects.all().count(), 1)

        #post
        response = self.client.post(reverse('workspace_delete',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}), follow=True)

        self.assertEqual(Workspace.objects.all().count(), 1)

        self.assertEqual(response.status_code, 404)
        self.assertContains(response, 'alert-success', 0, 404)
        self.assertContains(response, 'alert-error', 0, 404)

    def test_workspace_delete_guest_get(self):

        #get
        response = self.client.get(reverse('workspace_delete',
                                           kwargs={'username': self.user_admin.username,
                                                   'uuid': self.workspace.uuid}))

        self.assertEqual(response.status_code, 302)

    def test_workspace_delete_guest_post(self):

        self.assertEqual(Workspace.objects.all().count(), 1)

        #post
        response = self.client.post(reverse('workspace_delete',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.workspace.uuid}))

        self.assertEqual(Workspace.objects.all().count(), 1)

        self.assertEqual(response.status_code, 302)
        self.assertContains(response, 'alert-success', 0, 302)
        self.assertContains(response, 'alert-error', 0, 302)


    def test_update_goal_admin(self):

        response = self.client.post(reverse('auth_login'),
                                    {'username': self.user_admin_username,
                                     'password': self.user_admin_username,
                                     }, follow=True)

        self.assertRedirects(response,
                             build_test_url(reverse('goals_list', kwargs={'username': self.user_admin.username})))

        response = self.client.post(reverse('goal_update',
                                            kwargs={'username': self.user_admin.username,
                                                    'uuid': self.goal.uuid}),
                                    {'title': self.goal_title,
                                     'description': self.goal_description,
                                     'workspaces': self.workspace.pk,
                                     'snippet_title_0': self.snippet_title_0,
                                     'snippet_language_0': self.snippet_language_0,
                                     'snippet_text_0': self.snippet_text_0,
                                     'snippet_count': 1,
                                     'is_private': False},
                                    follow=True)

    """def test_remove_workspace_subscriber(self):
        self.client.post(reverse('auth_login'),
                         {'username': 'rene1',
                          'password': 'rene1',
                          })

        response = self.client.post(reverse('workspace_detail',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': self.workspace_user.uuid},
        ),
                                    {'workspace_remove_id': self.workspace_user.uuid})

        self.assertEqual(response.status_code, 404)
        self.assertEqual(Workspace.objects.all().count(), 1)

    def test_remove_workspace_editor(self):
        self.client.post(reverse('auth_login'),
                         {'username': 'rene2',
                          'password': 'rene2',
                         })

        response = self.client.post(reverse('workspaces_list',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': self.workspace_user.uuid},
        ),
                                    {'workspace_remove_id': self.workspace_user.uuid})

        self.assertEqual(response.status_code, 404)
        self.assertEqual(Workspace.objects.all().count(), 1)

    def test_update_workspace_subscriber(self):
        self.client.post(reverse('auth_login'),
                         {'username': 'rene1',
                          'password': 'rene1',
                         })

        response = self.client.get(reverse('workspace_update',
                                           kwargs={'username': self.new_user.username,
                                                   'uuid': self.workspace_user.uuid},
        ))

        self.assertEqual(response.status_code, 404)

        response = self.client.post(reverse('workspace_update',
                                            kwargs={'username': self.new_user.username,
                                                    'uuid': self.workspace_user.uuid},
        ),
                                    {'title': "New Title", "description": "New Description"})

        self.assertEqual(response.status_code, 404)
        self.assertEqual(Workspace.objects.all().count(), 1)"""




