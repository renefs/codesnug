# -*- coding: utf-8 -*-
from django.db import models
from codesnug.users.models import MyUser
from codesnug.users.models import UserGroup
from codesnug.settings import WORKSPACE_PERMISSIONS, LANGUAGE_CHOICES

from uuidfield import UUIDField


class Tag(models.Model):
    title = models.CharField(max_length=50, blank=False)
    owner = models.ForeignKey(MyUser, related_name='owned_tags')
    uuid = UUIDField(auto=True)

    def __unicode__(self):
        return self.title

    class Meta:
        unique_together = (("title", "owner"),)
        ordering = ('title', )


class Workspace(models.Model):
    owner = models.ForeignKey(MyUser, related_name='owned_workspaces')
    title = models.CharField(max_length=200, blank=False)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    uuid = UUIDField(auto=True)

    def __unicode__(self):
        return self.title

    class Meta:
        unique_together = (("title", "owner"),)
        ordering = ('title', )


class WorkspacePermissions(models.Model):

    group = models.ForeignKey(UserGroup, related_name='usergroups_permissions')
    workspace = models.ForeignKey(Workspace, related_name='usergroups_permissions')
    permission = models.IntegerField(max_length=3, choices=WORKSPACE_PERMISSIONS)
    uuid = UUIDField(auto=True)

    class Meta:
        unique_together = (("group", "workspace"),)


class Goal(models.Model):
    owner = models.ForeignKey(MyUser, related_name='owned_goals')
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField(Tag, blank=True, related_name='goals')
    workspaces = models.ManyToManyField(Workspace, blank=True, related_name='goals')
    is_private = models.BooleanField(default=True)
    uuid = UUIDField(auto=True)

    class Meta:
        unique_together = (("title", "owner"),)
        ordering = ('created_at', )

    def __unicode__(self):
        return self.uuid.__unicode__()

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        url = reverse("goal_detail", args=[str(self.uuid), str(self.owner.username)])
        print url
        return url


class BaseSnippet(models.Model):
    language = models.CharField(max_length=100, choices=LANGUAGE_CHOICES)
    version_number = models.IntegerField(default=1)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    uuid = UUIDField(auto=True)

    def __unicode__(self):
        return self.uuid.__unicode__()

    class Meta:
        abstract = True
        ordering = ('created_at', )


class Snippet(BaseSnippet):
    author = models.ForeignKey(MyUser, related_name='owned_snippets')
    goal = models.ForeignKey(Goal, related_name='snippets')
    title = models.CharField(max_length=200)


class Version(BaseSnippet):
    author = models.ForeignKey(MyUser, related_name='owned_versions')
    snippet = models.ForeignKey(Snippet, related_name='versions')

    class Meta:
        unique_together = (("snippet", "version_number"),)