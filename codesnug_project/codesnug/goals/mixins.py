from codesnug.goals.models import Goal
from codesnug.utils import valid_uuid

__author__ = 'rene'


class RemoveGoalFromListMixin(object):
    def remove_goal_from_list(self, goal_list, request):
        goal_remove_id = request.POST.get('goal_remove_id', None)
        if not valid_uuid(goal_remove_id):
            return None
        try:
            goal = Goal.objects.get(owner=self.request.user, uuid=goal_remove_id)
        except Goal.DoesNotExist:
            goal = None

        if goal is not None:
            goal_list.remove(goal)
            return True
        return None

class CheckOwnerMixin(object):
    def is_owner_current_user(self):
        if self.request.user.pk != self.get_object().owner.pk:
            return None
        return True