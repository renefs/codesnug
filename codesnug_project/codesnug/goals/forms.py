__author__ = 'rene'
from django import forms
from models import Goal, Snippet, Tag, Workspace
from codesnug.settings import LANGUAGE_CHOICES, SELECTABLE_WORKSPACE_PERMISSIONS


from haystack.forms import SearchForm


class WorkspacePermissionsForm(forms.Form):
    workspace = forms.ModelChoiceField(queryset=Workspace.objects.none())
    permission = forms.ChoiceField(choices=SELECTABLE_WORKSPACE_PERMISSIONS)
    usergroup = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, user, *args, **kwargs):
        super(WorkspacePermissionsForm, self).__init__(*args, **kwargs)
        self.fields['workspace'].queryset = Workspace.objects.filter(owner=user)


class WorkspaceUpdateForm(forms.ModelForm):
    class Meta:
        model = Workspace
        exclude = ['owner', 'created_at', 'uuid']


class GoalForm(forms.ModelForm):
    workspaces = forms.ModelMultipleChoiceField(queryset=Workspace.objects.none(),
                                                required=False, widget=forms.CheckboxSelectMultiple())
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.none(),
                                                required=False, widget=forms.CheckboxSelectMultiple())
    snippet_count = forms.CharField(widget=forms.HiddenInput())

    snippet_title_0 = forms.CharField(max_length=100)
    snippet_language_0 = forms.ChoiceField(choices=LANGUAGE_CHOICES)
    snippet_text_0 = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Goal
        exclude = ['owner', 'created_at']

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        number_of_snippets = kwargs.pop('snippet_count', 1)
        saved_goal = kwargs.pop('saved_goal', None)

        print "FORM: user: " + str(user) + " number of snippets: " + str(number_of_snippets)

        super(GoalForm, self).__init__(*args, **kwargs)
        self.fields['workspaces'].queryset = Workspace.objects.filter(owner=user)
        self.fields['tags'].queryset = Tag.objects.filter(owner=user)

        self.fields['snippet_count'].initial = number_of_snippets
        snippets_range = range(int(number_of_snippets))
        if saved_goal:
            snippets_range = range(0, int(number_of_snippets))

        for index in snippets_range:
            # generate extra fields in the number specified via extra_fields
            self.fields['snippet_title_{index}'.format(index=index)] = forms.CharField(max_length=100)
            if saved_goal:
                self.fields['snippet_title_{index}'.format(index=index)].initial = saved_goal.snippets.all()[index].title
            self.fields['snippet_language_{index}'.format(index=index)] = forms.ChoiceField(choices=LANGUAGE_CHOICES)
            if saved_goal:
                self.fields['snippet_language_{index}'.format(index=index)].initial = saved_goal.snippets.all()[index].language
            self.fields['snippet_text_{index}'.format(index=index)] = forms.CharField(widget=forms.Textarea)
            if saved_goal:
                self.fields['snippet_text_{index}'.format(index=index)].initial = saved_goal.snippets.all()[index].text


    def clean(self):
        print self.cleaned_data

        return self.cleaned_data

