from django.shortcuts import render

# Create your views here.
from haystack.query import SearchQuerySet
from haystack.views import SearchView
from codesnug.goals.models import WorkspacePermissions


class GoalSearchResults(SearchView):
    """
    Search > Root
    """
    template = 'search/search_root.html'

    def user_can_view_goal(self, goal, user):
        print "can view?"
        if goal.owner == user:
            print "owner is user"
            return True

        try:
            workspace_permission = WorkspacePermissions.objects.get(workspace=goal.workspaces.all, group__users=user)
        except WorkspacePermissions.DoesNotExist:
            return False
        if workspace_permission and not goal.is_private:
            return True
        return False

    def build_form(self, form_kwargs=None):
        """
        Instantiates the form the class should use to process the search query.
        """
        data = None
        kwargs = {
            'load_all': self.load_all,
            }
        if form_kwargs:
            kwargs.update(form_kwargs)

        if len(self.request.GET):
            data = self.request.GET

        self.searchqueryset = SearchQuerySet().all()

        """for goal in self.searchqueryset:
            print goal.title"""

        if self.searchqueryset is not None:
            kwargs['searchqueryset'] = self.searchqueryset

        return self.form_class(data, **kwargs)

    def extra_context(self):
        """
        Allows the addition of more context variables as needed.

        Must return a dictionary.
        """
        results_list = list(self.results)
        #print results_list
        if self.searchqueryset is not None:
            for result in self.results:
                print result.object.get_absolute_url
                if not self.user_can_view_goal(result.object, self.request.user):
                    results_list.remove(result)
                    print "can not view goal " + result.object.title
                print results_list
            return {'total_count': len(results_list), 'results': results_list}
        else:
            return {}