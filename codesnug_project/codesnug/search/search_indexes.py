__author__ = 'rene'

from haystack import indexes
from codesnug.goals.models import Goal
import datetime


class GoalIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    description = indexes.CharField(model_attr='description')
    created_at = indexes.DateTimeField(model_attr='created_at')
    owner = indexes.CharField(model_attr='owner__username')
    uuid = indexes.CharField(model_attr='uuid')
    snippets_count = indexes.IntegerField(model_attr='snippets__count')

    def get_model(self):
        return Goal

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(created_at__lte=datetime.datetime.now())