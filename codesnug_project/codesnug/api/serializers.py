__author__ = 'rene'


from rest_framework import serializers

from codesnug.goals.models import Goal, Snippet, Workspace, Tag
from codesnug.users.models import MyUser


class GoalSerializer(serializers.HyperlinkedModelSerializer):

    owner = serializers.RelatedField(many=False)
    snippets = serializers.RelatedField(many=True)
    workspaces = serializers.RelatedField(many=True)
    tags = serializers.RelatedField(many=True)

    class Meta:
        model = Goal
        fields = ('title', 'description', 'uuid', 'created_at', 'is_private', 'owner', 'snippets', 'workspaces', 'tags')


class SnippetSerializer(serializers.HyperlinkedModelSerializer):

    goal = serializers.RelatedField(many=False, source='goal.uuid')
    author = serializers.RelatedField(many=False)
    versions = serializers.RelatedField(many=True)

    class Meta:
        model = Snippet
        fields = ('title', 'goal', 'author', 'text', 'created_at', 'uuid', 'language', 'version_number', 'versions')


class WorkspaceSerializer(serializers.HyperlinkedModelSerializer):

    owner = serializers.RelatedField(many=False)
    goals = serializers.RelatedField(many=True)

    class Meta:
        model = Workspace
        fields = ('title', 'description', 'created_at', 'owner', 'uuid', 'goals')


class TagSerializer(serializers.HyperlinkedModelSerializer):

    owner = serializers.RelatedField(many=False)
    goals = serializers.RelatedField(many=True)

    class Meta:
        model = Tag
        fields = ('title', 'owner', 'uuid', 'goals')


class MyUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MyUser
        fields = ('username','uuid')