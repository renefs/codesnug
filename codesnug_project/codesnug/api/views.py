from rest_framework.response import Response

__author__ = 'rene'
from codesnug.goals.models import Goal, Snippet, Workspace, Tag
from rest_framework import viewsets
from codesnug.api.serializers import GoalSerializer, SnippetSerializer, WorkspaceSerializer, TagSerializer

from codesnug.users.models import MyUser
from codesnug.api.serializers import MyUserSerializer


class WorkspaceViewSet(viewsets.ModelViewSet):
    serializer_class = WorkspaceSerializer
    lookup_field = 'uuid'

    def get_queryset(self):
        return Workspace.objects.filter(owner=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        print "Retrieve workspace"
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)


class TagViewSet(viewsets.ModelViewSet):
    serializer_class = TagSerializer
    lookup_field = 'uuid'

    def get_queryset(self):
        return Tag.objects.filter(owner=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        print "Retrieve tag"
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)


class GoalViewSet(viewsets.ModelViewSet):
    serializer_class = GoalSerializer
    lookup_field = 'uuid'

    def get_queryset(self):
        return Goal.objects.filter(owner=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        print "Retrieve goal"
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)


class SnippetViewSet(viewsets.ModelViewSet):
    serializer_class = SnippetSerializer
    lookup_field = 'uuid'

    def get_queryset(self):
        return Snippet.objects.filter(goal__owner=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        print "Retrieve snippet"
        self.object = self.get_object()
        serializer = self.get_serializer(self.object)
        return Response(serializer.data)


class MyUserViewSet(viewsets.ModelViewSet):

    serializer_class = MyUserSerializer
    lookup_field = 'username'

    def get_queryset(self):
        return MyUser.objects.all()
