from django.conf.urls import patterns, include, url
from codesnug.users.forms import UserRegistrationForm
from registration.backends.default.views import RegistrationView
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import views as auth_views
from goals.decorators import add_workspace_ajax, add_tag_ajax
from users.decorators import update_user_first_name, update_user_last_name, update_user_email, update_user_birth_date, \
    update_user_password, update_workspace_permission

from codesnug.views import Home, TermsConditions, Faqs, Api, TheProject, Privacy, ContactUs, WhoWeAre

from codesnug.goals.views import WorkspaceList, WorkspaceCreate, WorkspacePermissionsCreate, \
    GoalCreate, WorkspaceDetail, GoalList, GoalDetail, GoalUpdate, GoalDelete, WorkspaceUpdate, WorkspaceDelete, TagList, \
    TagCreate, TagDetail, TagUpdate, TagDelete, SnippetDetail, DataDownload, SnippetDownload, GoalDownload

from codesnug.search.views import GoalSearchResults

from codesnug.users.views import UserGroupList, UserGroupCreate, UserGroupDetail, \
    AddUserToGroupView, Profile, UserGroupUpdate, UserGroupDelete

from haystack.views import SearchView, search_view_factory
from codesnug.search.forms import GoalSearchForm

from rest_framework import routers

from codesnug.api.views import GoalViewSet, SnippetViewSet, MyUserViewSet, WorkspaceViewSet, TagViewSet

admin.autodiscover()

urlpatterns = patterns('',
                       # pages
                       url(r'^$', Home.as_view(),
                           name='home'),
                       url(r'^help/faqs/$', Faqs.as_view(),
                           name='help_faqs'),
                       url(r'^help/api/$', Api.as_view(),
                           name='help_api'),
                       url(r'^about/the-project/$', TheProject.as_view(),
                           name='the_project'),
                       url(r'^about/terms-and-conditions/$', TermsConditions.as_view(),
                           name='terms_conditions'),
                       url(r'^about/privacy/$', Privacy.as_view(),
                           name='privacy'),
                       url(r'^contact/$', ContactUs.as_view(),
                           name='contact_us'),
                       url(r'^contact/who-we-are/$', WhoWeAre.as_view(),
                           name='who_we_are'),
                       )

urlpatterns += patterns('',
                        # built-in
                        (r'^i18n/', include('django.conf.urls.i18n')),
                        url(r'^admin/', include(admin.site.urls)),
                        )

urlpatterns += patterns('',
                        # Registration
                        url(r'^accounts/profile/$', Profile.as_view(),
                            name='profile'),
                        url(r'^accounts/register/$', RegistrationView.as_view(form_class=UserRegistrationForm),
                            name='registration_register'),
                        (r'^accounts/', include('registration.backends.default.urls')),
                        url(r'^login/$', auth_views.login, {'template_name': 'registration/login.html'},
                            name='auth_login'),
                        url(r'^logout/$', auth_views.logout, {'template_name': 'registration/logout.html'},
                            name='auth_logout'),
                        url(r'^password/change/$', auth_views.password_change,
                            {'post_change_redirect': reverse_lazy('password_change_done')},
                            name='password_change'),
                        url(r'^password/change/done/$', auth_views.password_change_done,
                            name='password_change_done'),
                        url(r'^password/reset/$', auth_views.password_reset,
                            {'post_reset_redirect': reverse_lazy('password_reset_done')},
                            name='password_reset'),
                        url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
                            auth_views.password_reset_confirm,
                            name='password_reset_confirm'),
                        url(r'^password/reset/complete/$', auth_views.password_reset_complete,
                            {'post_reset_redirect': reverse_lazy('password_reset_complete')},
                            name='password_reset_complete'),
                        url(r'^password/reset/done/$', auth_views.password_reset_done,
                            name='password_reset_done'),
                        )

urlpatterns += patterns('',
                        # Users
                        url(r'^edit_first_name$', update_user_first_name,
                            name='edit_first_name_ajax'),
                        url(r'^edit_last_name$', update_user_last_name,
                            name='edit_last_name_ajax'),
                        url(r'^edit_email$', update_user_email,
                            name='edit_email_ajax'),
                        url(r'^edit_birth_date$', update_user_birth_date,
                            name='edit_birth_date_ajax'),
                        url(r'^edit_password$', update_user_password,
                            name='edit_password_ajax'),

                        #Download Data
                        url(r'^download-data/$', DataDownload.as_view(),
                            name='download_data'),
                        url(r'^(?P<username>[^/]+)/download-snippet/(?P<uuid>[^/]+)/$', SnippetDownload.as_view(),
                            name='download_snippet'),
                        url(r'^(?P<username>[^/]+)/download-goal/(?P<uuid>[^/]+)/$', GoalDownload.as_view(),
                            name='download_goal'),

                        #UserGroups
                        url(r'^app/(?P<username>[^/]+)/usergroups/$', UserGroupList.as_view(),
                            name='usergroups_list'),
                        url(r'^usergroups/add/$', UserGroupCreate.as_view(),
                            name='usergroups_add'),
                        url(r'^app/(?P<username>[^/]+)/usergroups/(?P<uuid>[^/]+)/$', UserGroupDetail.as_view(),
                            name='usergroup_detail'),
                        url(r'^app/(?P<username>[^/]+)/usergroups/(?P<uuid>[^/]+)/edit/$', UserGroupUpdate.as_view(),
                            name='usergroup_update'),
                        url(r'^app/(?P<username>[^/]+)/usergroups/(?P<uuid>[^/]+)/delete/$', UserGroupDelete.as_view(),
                            name='usergroup_delete'),
                        url(r'^app/(?P<username>[^/]+)/usergroups/(?P<uuid>[^/]+)/add-workspace/$',
                            WorkspacePermissionsCreate.as_view(),
                            name='asign_workspace_group'),
                        url(r'^app/(?P<username>[^/]+)/usergroups/(?P<uuid>[^/]+)/add-user/$', AddUserToGroupView.as_view(),
                            name='asign_user_group'),

                        url(r'^edit_workspace_permission$', update_workspace_permission,
                            name='edit_workspace_permission'),
                        )

urlpatterns += patterns('',
                        # goals
                        url(r'^app/(?P<username>[^/]+)/goals/$', GoalList.as_view(),
                            name='goals_list'),
                        url(r'^goals/add/$', GoalCreate.as_view(),
                            name='goals_add'),
                        url(r'^app/(?P<username>[^/]+)/goals/(?P<uuid>[^/]+)/$', GoalDetail.as_view(),
                            name='goal_detail'),
                        url(r'^app/(?P<username>[^/]+)/goals/(?P<goal_uuid>[^/]+)/snippet/(?P<uuid>[^/]+)/$',
                            SnippetDetail.as_view(),
                            name='snippet_detail'),
                        url(r'^app/(?P<username>[^/]+)/goals/(?P<uuid>[^/]+)/edit/$', GoalUpdate.as_view(),
                            name='goal_update'),
                        url(r'^app/(?P<username>[^/]+)/goals/(?P<uuid>[^/]+)/delete/$', GoalDelete.as_view(),
                            name='goal_delete'),

                        #goals search
                        #(r'^search/', include('haystack.urls')),
                        #url(r'^search-test/$', GoalSearchResults.as_view(), name='search_test'),


                        #Workspaces
                        url(r'^app/(?P<username>[^/]+)/workspaces/$', WorkspaceList.as_view(),
                            name='workspaces_list'),
                        url(r'^workspaces/add/$', WorkspaceCreate.as_view(),
                            name='workspaces_add'),
                        url(r'^app/(?P<username>[^/]+)/workspaces/(?P<uuid>[^/]+)/$', WorkspaceDetail.as_view(),
                            name='workspace_detail'),
                        url(r'^app/(?P<username>[^/]+)/workspaces/(?P<uuid>[^/]+)/edit/$', WorkspaceUpdate.as_view(),
                            name='workspace_update'),
                        url(r'^app/(?P<username>[^/]+)/workspaces/(?P<uuid>[^/]+)/delete/$', WorkspaceDelete.as_view(),
                            name='workspace_delete'),
                        url(r'^add-workspace$', add_workspace_ajax,
                            name='workspace_add_ajax'),

                        #Tags
                        url(r'^app/(?P<username>[^/]+)/tags/$', TagList.as_view(),
                            name='tags_list'),
                        url(r'^tags/add/$', TagCreate.as_view(),
                            name='tags_add'),
                        url(r'^app/(?P<username>[^/]+)/tags/(?P<uuid>[^/]+)/$', TagDetail.as_view(),
                            name='tag_detail'),
                        url(r'^app/(?P<username>[^/]+)/tags/(?P<uuid>[^/]+)/edit/$', TagUpdate.as_view(),
                            name='tag_update'),
                        url(r'^app/(?P<username>[^/]+)/tags/(?P<uuid>[^/]+)/delete/$', TagDelete.as_view(),
                            name='tag_delete'),
                        url(r'^add-tag$', add_tag_ajax,
                            name='tag_add_ajax'),
                        )

urlpatterns += patterns('',
                        # Modules
                        (r'^selectable/', include('selectable.urls')),
                        )

urlpatterns += patterns('haystack.views',
                        url(r'^search/$', search_view_factory(
                           view_class=GoalSearchResults,
                           #template='my/special/path/john_search.html',
                           #searchqueryset=sqs,
                           form_class=GoalSearchForm
                        ), name='search_test'),
                       )

router = routers.DefaultRouter()

"""goal_list = GoalViewSet.as_view({
    'get': 'list',
})

router.register(r'goal', GoalViewSet.as_view({
    'get': 'list',
    })
    , base_name='goal-list')"""

urlpatterns += patterns('',
                        url(r'^api/', include(router.urls), name='api_root'),
                        #Tags
                        url(r'^api/tags/$', TagViewSet.as_view({
                            'get': 'list',
                            })
                            , name='tag-list'),
                        url(r'^api/tags/(?P<uuid>[^/]+)$', TagViewSet.as_view({
                            'get': 'retrieve',
                            })
                            , name='tag-detail'),
                        #Tags
                        #Workspace
                        url(r'^api/workspaces/$', WorkspaceViewSet.as_view({
                            'get': 'list',
                            })
                            , name='workspace-list'),
                        url(r'^api/workspaces/(?P<uuid>[^/]+)$', WorkspaceViewSet.as_view({
                            'get': 'retrieve',
                            })
                            , name='workspace-detail'),
                        #Workspace
                        #Goal
                        url(r'^api/goals/$', GoalViewSet.as_view({
                            'get': 'list',
                            })
                            , name='goal-list'),
                        url(r'^api/goals/(?P<uuid>[^/]+)$', GoalViewSet.as_view({
                            'get': 'retrieve',
                            })
                            , name='goal-detail'),

                        url(r'^api/users/(?P<username>[^/]+)$', MyUserViewSet.as_view({
                            'get': 'retrieve',
                            })
                            , name='myuser-detail'),
                        #Goal
                        #Snippet
                        url(r'^api/snippets/$', SnippetViewSet.as_view({
                            'get': 'list',
                            })
                            , name='goal-list'),
                        url(r'^api/snippets/(?P<uuid>[^/]+)$', SnippetViewSet.as_view({
                            'get': 'retrieve',
                            })
                            , name='snippet-detail'),
                        #Snippet
                        url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
                        )

